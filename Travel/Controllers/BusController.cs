﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Travel.Models;

namespace Travel.Controllers
{
    public class BusController : ApiController
    {
        public static string Message = "";
        readonly Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddBus(BusDTO busDto)
        {
            try
            {

                var busId = DAL.AddBus(busDto);
                if (busId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = busId;
                    Result.Message = CustomConstants.Bus_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddBid(BidDTO bidDto)
        {
            try
            {

                var bidId = DAL.AddBid(bidDto);
                if (bidId == -99)
                {
                    
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = bidId;
                    Result.Message = CustomConstants.Record_Not_Inserted;

                }
                else if(bidId == 100)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = 0;
                    Result.Message = "There is no service running for this route";
                }
                else
                {
                    var getBidDetails = DAL.GetLatestBid(bidDto.CustomerId);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = getBidDetails;
                    Result.Message = CustomConstants.Bid_Added_Successfully;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddService(ServiceDTO serviceDto)
        {
            try
            {

                int serviceId = DAL.AddService(serviceDto);
                DateTime dtFrom = serviceDto.FromDate;
                DateTime dtTo = serviceDto.ToDate;
                if (serviceDto.DayTypeId == 1)
                {
                    var serviceDaysDto = new ServiceDaysDTO();
                    DateTime dailyDate = serviceDto.FromDate;
                    while (dailyDate <= serviceDto.ToDate)
                    {
                        serviceDaysDto.ServiceId = serviceId;
                        serviceDaysDto.Date = dailyDate;
                        serviceDaysDto.Time = serviceDto.FromTime;
                        DAL.AddServiceDays(serviceDaysDto);
                        dailyDate = dailyDate.AddDays(1);
                    }
                }
                else
                {
                    List<int> days = serviceDto.DayTypeId == 2 ? new List<int> { 1, 2, 3, 4 } : (serviceDto.DayTypeId == 3 ? new List<int> { 5, 6, 0 } : (serviceDto.DayTypeId == 4 ? new List<int> { serviceDto.serviceDay } : new List<int>()));
                    //check if daytypeid==5 then add single date to servicedays
                    var serviceDaysDto = new ServiceDaysDTO();
                    var dailyDate = serviceDto.FromDate;
                    if (serviceDto.DayTypeId == 5)
                    {
                        serviceDaysDto.ServiceId = serviceId;
                        serviceDaysDto.Date = dailyDate;
                        serviceDaysDto.Time = serviceDto.FromTime;
                        DAL.AddServiceDays(serviceDaysDto);
                    }
                    else
                    {
                        var objDatestoFeed = GetDates(dtFrom, dtTo, days);
                        foreach (var dt in objDatestoFeed)
                        {
                            serviceDaysDto.ServiceId = serviceId;
                            serviceDaysDto.Date = dt;
                            serviceDaysDto.Time = serviceDto.FromTime;
                            DAL.AddServiceDays(serviceDaysDto);
                        }

                    }
                }

                if (serviceId > 0)
                {
                    //Add Service Stops here 
                    ServiceStopsDTO serviceStops = new ServiceStopsDTO();
                    ServiceAmenitiesDTO ServiceAmenities = new ServiceAmenitiesDTO();
                    for (int i = 0; i < serviceDto.Locations.Count; i++)
                    {
                        serviceStops.ServiceId = serviceId;
                        serviceStops.LocationId = serviceDto.Locations[i];
                        DAL.AddServiceStops(serviceStops);
                    }
                    for (int i = 0; i < serviceDto.Amenities.Count; i++)
                    {
                        ServiceAmenities.ServiceId = serviceId;
                        ServiceAmenities.AmenityId = serviceDto.Amenities[i];
                        DAL.AddServiceAmenities(ServiceAmenities);
                    }
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceId;
                    Result.Message = CustomConstants.Service_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        private static List<DateTime> GetDates(DateTime fromDate, DateTime toDate, ICollection<int> days)
        {
            var objDates = new List<DateTime>();
            for (int i = 0; i <= toDate.Subtract(fromDate).Days; i++)
            {
                if (days.Contains(Convert.ToInt32(fromDate.Date.AddDays(i).DayOfWeek)))
                    objDates.Add(fromDate.Date.AddDays(i));
            }
            return objDates;
        }



        [HttpPost]
        public Utility.CustomResponse EditService(ServiceDTO serviceDto)
        {
            try
            {

                var serviceId = DAL.AddService(serviceDto);
                var dtFrom = serviceDto.FromDate;
                var dtTo = serviceDto.ToDate;
                switch (serviceDto.DayTypeId)
                {
                    case 1:
                    {
                        var serviceDaysDto = new ServiceDaysDTO();
                        DateTime dailyDate = serviceDto.FromDate;
                        while (dailyDate <= serviceDto.ToDate)
                        {
                            serviceDaysDto.ServiceId = serviceId;
                            serviceDaysDto.Date = dailyDate;
                            serviceDaysDto.Time = serviceDto.FromTime;
                            DAL.AddServiceDays(serviceDaysDto);
                            dailyDate = dailyDate.AddDays(1);
                        }
                    }
                        break;
                    default:
                    {
                        var days = serviceDto.DayTypeId == 2 ? new List<int> { 1, 2, 3, 4 } : (serviceDto.DayTypeId == 3 ? new List<int> { 5, 6, 0 } : (serviceDto.DayTypeId == 4 ? new List<int> { serviceDto.serviceDay } : new List<int>()));
                        //check if daytypeid==5 then add single date to servicedays
                        var serviceDaysDto = new ServiceDaysDTO();
                        var dailyDate = serviceDto.FromDate;
                        if (serviceDto.DayTypeId == 5)
                        {
                            serviceDaysDto.ServiceId = serviceId;
                            serviceDaysDto.Date = dailyDate;
                            serviceDaysDto.Time = serviceDto.FromTime;
                            DAL.AddServiceDays(serviceDaysDto);
                        }
                        else
                        {
                            var objDatestoFeed = GetDates(dtFrom, dtTo, days);
                            foreach (var dt in objDatestoFeed)
                            {
                                serviceDaysDto.ServiceId = serviceId;
                                serviceDaysDto.Date = dt;
                                serviceDaysDto.Time = serviceDto.FromTime;
                                DAL.AddServiceDays(serviceDaysDto);
                            }

                        }
                    }
                        break;
                }

                if (serviceId > 0)
                {
                    //Delete Service Stops
                    foreach (var i in serviceDto.DeleteLocations)
                    {
                        DAL.DeleteServiceStops(serviceDto.Id, Convert.ToInt32(i));
                    }
                    //Delete Service Amenities
                    foreach (var i in serviceDto.DeleteAmenities)
                    {
                        DAL.DeleteServiceAmenities(serviceDto.Id, Convert.ToInt32(i));
                    }
                    //Add Service Stops here 
                    ServiceStopsDTO serviceStops = new ServiceStopsDTO();
                    ServiceAmenitiesDTO serviceAmenities = new ServiceAmenitiesDTO();
                    foreach (var t in serviceDto.Locations)
                    {
                        serviceStops.ServiceId = serviceId;
                        serviceStops.LocationId = t;
                        DAL.AddServiceStops(serviceStops);
                    }
                    foreach (var t in serviceDto.Amenities)
                    {
                        serviceAmenities.ServiceId = serviceId;
                        serviceAmenities.AmenityId = t;
                        DAL.AddServiceAmenities(serviceAmenities);
                    }
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceId;
                    Result.Message = CustomConstants.Service_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddServiceDays(ServiceDaysDTO serviceDaysDTO)
        {
            try
            {

                int serviceDaysId = DAL.AddServiceDays(serviceDaysDTO);
                if (serviceDaysId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceDaysId;
                    Result.Message = CustomConstants.ServiceDays_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddServiceAmenities(ServiceAmenitiesDTO serviceAmenitiesDto)
        {
            try
            {

                var serviceAmenityId = DAL.AddServiceAmenities(serviceAmenitiesDto);
                if (serviceAmenityId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (serviceAmenityId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceAmenityId;
                    Result.Message = CustomConstants.ServiceAmenities_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddServiceStops(ServiceStopsDTO serviceStopsDto)
        {
            try
            {
                int serviceStopsId = DAL.AddServiceStops(serviceStopsDto);

                switch (serviceStopsId)
                {
                    case -99:
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.Already_Exists;
                        break;
                    default:
                        if (serviceStopsId > 0)
                        {
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = serviceStopsId;
                            Result.Message = CustomConstants.ServiceStops_Added_Successfully;
                        }
                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.AccessDenied;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ServiceAmenities()
        {
            try
            {

                List<ServiceAmenitiesDTO> serviceAmenities = DAL.GetServiceAmenities(0);
                if (serviceAmenities.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceAmenities;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse Buses()
        {
            try
            {

                var buses = DAL.GetBuses();
                if (buses.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = buses;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse Services(int routeId, DateTime date)
        {
            try
            {

                var services = DAL.GetService(routeId, date);
                if (services.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = services;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse ServiceById(int ServiceId)
        {
            try
            {

                ServiceDTO services = DAL.GetServiceById(ServiceId);
                services.Amenities = DAL.GetAmenitiesForService(ServiceId);
                services.Locations = DAL.GetLocationsForService(ServiceId);

                if (services.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = services;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteService(int ServiceId)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Utility.CustomResponse();

            try
            {

                var serviceId = DAL.DeleteService(ServiceId);
                if (serviceId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = serviceId;
                    Result.Message = CustomConstants.Service_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse ServicesByMerchants(int merchantId)
        {
            try
            {
                var services = DAL.GetServiceByMerchants(merchantId);
                if (services.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = services;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteBus(int id)
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Utility.CustomResponse();

            try
            {

                var busId = DAL.DeleteBus(id);
                if (busId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = busId;
                    Result.Message = CustomConstants.Bus_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CancelBid(CancelBidsDTO cancelBidsDto)
        {
            try
            {

                var cancelbidId = DAL.CancelBid(cancelBidsDto);

                if (cancelbidId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = cancelbidId;
                    Result.Message = CustomConstants.Data_Saved_Successfully;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetCancelBids(int userId)
        {
            try
            {

                var services = DAL.GetCancelBids(userId);
                if (services.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = services;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetBusCategories()
        {
            try
            {

                List<BusCategoriesDTO> categories = DAL.GetBusCategories();
                if (categories.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = categories;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
