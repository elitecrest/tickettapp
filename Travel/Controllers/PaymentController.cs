﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Travel.Models;

namespace Travel.Controllers
{

    public class PaymentController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddTransaction(TransactionDTO transactionDTO)
        {
            try
            {

                int TransactionId = DAL.AddTransaction(transactionDTO);
                if (TransactionId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = TransactionId;
                    Result.Message = CustomConstants.Transaction_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddMatchingBids(MatchingBidsDTO matchingBidsDTO)
        {
            try
            {

                int MatchingBidId = DAL.AddMatchingBids(matchingBidsDTO);
                if (MatchingBidId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = MatchingBidId;
                    Result.Message = CustomConstants.MatchingBids_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse UpdateSeatNumberandStatus(int SeatNo)
        {
            try
            {

                int SeatId = 0;// DAL.UpdateSeatNumberandStatus(SeatNo);
                if (SeatId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = SeatId;
                    Result.Message = CustomConstants.SeatNumber_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
