﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using Travel.Models;

namespace Travel.Controllers
{
    public class CustomerController : ApiController
    {
        public static string Message = "";
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetAllCustomers()
        {
            try
            {

                var customer = DAL.GetAllCustomers();
                if (customer.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = customer;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }



        [HttpGet]
        public Utility.CustomResponse GetCustomer(int customerId)
        {
            try
            {

                var customer = DAL.GetCustomer(customerId);
                if (customer.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = customer;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }






        [HttpPost]
        public Utility.CustomResponse AddCustomer(CustomerDTO customerDto)
        {
            try
            {
                if (customerDto.Id == 0)
                {
                    var custExists = DAL.CheckUserExists(customerDto.Email);
                    if (custExists == 0)
                    {
                        var customerId = DAL.AddCustomer(customerDto);
                        if (customerId > 0)
                        {
                            var customerInfo = DAL.GetCustomer(customerId);
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = customerInfo;
                            _result.Message = CustomConstants.Data_Saved_Successfully;
                        }
                        else
                        {
                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            _result.Message = CustomConstants.AccessDenied;
                        }
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = "User Already exists with this Email";
                        _result.Message = "User Already exists with this Email Address";
                    }
                }
                else
                {
                        var customerId = DAL.AddCustomer(customerDto);
                        if (customerId > 0)
                        {
                            var customerInfo = DAL.GetCustomer(customerId);
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = customerInfo;
                            _result.Message = CustomConstants.Data_Saved_Successfully;
                        }
                        else
                        {
                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            _result.Message = CustomConstants.AccessDenied;
                        }
                }
               
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse GetRelatedCustomers(int createdBy)
        {
            try
            {
                var locationsList = DAL.getRelatedCustomers(createdBy);
                if (locationsList.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = locationsList;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse GetBids(int customerid)
        {
            try
            {

                var bids = DAL.GetBids(customerid);
                if (bids.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = bids;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse GetBids_V1(int customerid)
        {
            try
            {

                var bids = DAL.GetBids_V1(customerid);
                if (bids.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = bids;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse AuthenticateUser(string emailId, string password)
        {
            try
            {

                var customerDto = DAL.authenticateUser(emailId, password);
                if (customerDto != null)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = customerDto;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }

        [HttpPost]
        public Utility.CustomResponse RegisterDevice(DeviceDTO deviceDto)
        {
            try
            {
                var deviceId = DAL.RegisterDevice(deviceDto);
                if (deviceId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Data_Saved_Successfully;
                    _result.Response = deviceId;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Registered_Failed;
                    _result.Response = deviceId;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }

        [HttpPost]
        public Utility.CustomResponse UploadSevices(List<ServiceExcelDTO> serviceExcelDto)
        {
            var checkResult = 0;
            try
            {
                List<ValidationSummaryDto> validateSummary = new List<ValidationSummaryDto>();
                ServiceExcelDTO service = new ServiceExcelDTO();

                foreach (var t in serviceExcelDto)
                {
                    ServiceDTO serviceDto = new ServiceDTO();
                    ValidationSummaryDto validate = new ValidationSummaryDto();
                    string[] words = t.Route.Trim().Split('-');
                    service.FromRoute = words[0];
                    service.ToRoute = words[1];
                    validate.ValidationErrorMessage = "Row-" + t.RowId + " is having invalid data:";
                    string validationErrorMessage = string.Empty;
                    var str1 = t.StartLocations.Aggregate(string.Empty, (current, t1) => !string.IsNullOrEmpty(current) ? current + "," + t1 : t1);
                    t.StartLocations2 = str1;
                    int fromCityId = 0, toCityId = 0, startLocationId = 0, endLocationId = 0, routeId = 0;
                    DAL.GetFromCityToCity(service.FromRoute, service.ToRoute, t.StartLocation, t.EndLocation, ref fromCityId, ref toCityId, ref startLocationId, ref endLocationId, ref routeId);
                    if (fromCityId == 0 || toCityId == 0)
                    {

                        validationErrorMessage = fromCityId == 0
                            ? validationErrorMessage
                              + ExcelUploadExceptionConstants.FromCityNotFound + ","
                            : validationErrorMessage
                              + ExcelUploadExceptionConstants.ToCityNotFound + ",";
                    }
                    else
                    {
                        t.FromCityId = fromCityId;
                        t.ToCityId = toCityId;
                    }

                    if (routeId == 0)
                    {
                        validationErrorMessage = validationErrorMessage
                                                          + ExcelUploadExceptionConstants.RouteNotFound + ",";

                    }
                    else
                    {
                        t.RouteId = routeId;
                    }
                    if (startLocationId == 0 || endLocationId == 0)
                    {
                        validationErrorMessage = startLocationId == 0
                            ? validationErrorMessage
                              + ExcelUploadExceptionConstants.StartLocationNotFound + ","
                            : validationErrorMessage
                              + ExcelUploadExceptionConstants.EndLocationNotFound + ",";

                    }
                    else
                    {
                        t.StartLocationId = startLocationId;
                        t.EndLocationId = endLocationId;
                    }
                    var startOutput = DAL.CheckForStartLocations(service.FromRoute, str1);
                    if (startOutput.Count > 0)
                    {
                        validationErrorMessage = validationErrorMessage
                              + ExcelUploadExceptionConstants.StartRouteNotFound + ",";
                    }
                    var str2 = t.EndLocations.Aggregate(string.Empty, (current, t1) => !string.IsNullOrEmpty(current) ? current + "," + t1 : t1);
                    t.EndLocations2 = str2;
                    var endOutput = DAL.CheckForStartLocations(service.ToRoute, str2);
                    if (endOutput.Count > 0)
                    {
                        validationErrorMessage = validationErrorMessage
                              + ExcelUploadExceptionConstants.EndRouteNotFound + ",";
                    }

                    int busTypeId = DAL.GetBusTypeId(t.BusType.Trim());
                    if (busTypeId == 0)
                    {
                        validationErrorMessage = validationErrorMessage +
                                                         ExcelUploadExceptionConstants.BusType + ",";

                    }
                    t.BusTypeId = busTypeId;
                    var amenitiesList = DAL.GetAmenitiesByBusType(busTypeId);
                    var dayTypeId = 0;
                    DAL.GetDayTypeId(t.DayType.Trim(), ref dayTypeId);
                    if (dayTypeId == 0)
                    {
                        validationErrorMessage = validationErrorMessage +
                              ExcelUploadExceptionConstants.DayType + ",";

                    }
                    if (t.DayType.Trim() == "Date")
                    {
                        if (t.FromDate < DateTime.Today)
                        {
                            validationErrorMessage = validationErrorMessage +
                                                       ExcelUploadExceptionConstants.FromDate + ",";
                        }
                    }


                    serviceDto.StartLocation = startLocationId;
                    serviceDto.EndLocation = endLocationId;
                    serviceDto.RouteId = routeId;
                    serviceDto.ServiceNumber = t.ServiceId;
                    serviceDto.FromTime = t.DeparuteTime;
                    serviceDto.ToTime = t.ArrivalTime;
                    serviceDto.NoOfStops = 0;
                    serviceDto.DayTypeId = dayTypeId; // Need to check this once
                    serviceDto.FromDate = t.FromDate;
                    serviceDto.ToDate = t.ToDate;
                    serviceDto.CreatedBy = t.CreatedBy;
                    serviceDto.Status = 1;
                    serviceDto.Type = 1;
                    serviceDto.VendorId = t.MerchantId;
                    serviceDto.StartLocation = startLocationId;
                    serviceDto.EndLocation = endLocationId;
                    serviceDto.RetailPrice = t.RetailPrice;
                    serviceDto.SpecialPrice = t.SpecialPrice;
                    serviceDto.BusTypeId = busTypeId;

                    if (validationErrorMessage.Length == 0)
                    {
                        validate.RowId = DAL.AddService(serviceDto);
                        if (t.StartLocations.Count > 0)
                        {
                            foreach (string t1 in t.StartLocations)
                            {
                                var output1 = DAL.AddServiceStops_v2(validate.RowId, t1, fromCityId);
                            }
                        }
                        if (t.EndLocations.Count > 0)
                        {
                            foreach (string t1 in t.EndLocations)
                            {
                                var output2 = DAL.AddServiceStops_v2(validate.RowId, t1, toCityId);
                            }
                        }
                        if (amenitiesList.Count > 0)
                        {
                            foreach (Int32 t1 in amenitiesList)
                            {
                                DAL.AddServiceAmenity(validate.RowId, t1, t.CreatedBy);
                            }
                        }
                        validate.ValidationErrorMessage = null;
                    }
                    else
                    {
                        var errorMEssage = validationErrorMessage.TrimEnd(',');
                        t.ValidationErrorMessages = errorMEssage;
                        validate.ValidationErrorMessage = validate.ValidationErrorMessage +
                                                          errorMEssage;
                        DAL.FailedExcelRecordValidation(t);
                        validate.RowId = 0;
                        checkResult++;

                    }
                    if (validate.ValidationErrorMessage != null)
                    {
                        var trimEnd = validate.ValidationErrorMessage.TrimEnd(',');
                    }
                    validateSummary.Add(validate);
                }
                if (checkResult == 0)
                {
                    _result.Status = 0;
                    _result.Message = ExcelUploadExceptionConstants.Success;
                    _result.Response = validateSummary;
                }
                else
                {
                    _result.Status = 0;
                    _result.Message = ExcelUploadExceptionConstants.ValidationFailure;
                    _result.Response = validateSummary;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string emailAddress)
        {

            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                string code = DAL.GetCustomerExists(emailAddress);
                if (code != string.Empty)
                {
                    string token = code;

                    //need to send a mail to user

                    if (SendMailForForgotPassword(emailAddress, token) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPassword_successfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }

            return res;
        }

        public static int SendMailForForgotPassword(string emailAddress, string token)
        {
            try
            {
                var reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                var readFile = reader.ReadToEnd();
                var myString = readFile;
                myString = myString.Replace("$$Name$$", emailAddress);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                var msg = new MailMessage();
                var fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"]);
                msg.From = fromMail;
                msg.To.Add(new MailAddress("" + emailAddress + ""));
                msg.Subject = " Forgot Password";
                msg.Body = myString;
                msg.IsBodyHtml = true;
                var smtpMail = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings["EmailHost"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password"]),
                    Timeout = 1000000
                };

                smtpMail.Send(msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                string email = DAL.UpdateCustomerPassword(userName, token, confirmPassword);
                if (email != string.Empty)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.ResetPassword_Failed;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse GetAdminUsers()
        {
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                var usersList = DAL.GetAdminUsersList();
                if (usersList.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = usersList;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpPost]
        public Utility.CustomResponse AddAdminUsers(AdminUserDTO adminUsersDto)
        {
            try
            {
                var output = DAL.AddAdminUsers(adminUsersDto);
            }

            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse FailedServiceHistory(int merchantId)
        {
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                var usersList = DAL.GetFailedServiceHistory(merchantId);
                if (usersList.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = usersList;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse GetAdminUsersById(int id)
        {
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                var usersList = DAL.GetAdminUsersById(id);
                if (usersList.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = usersList;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse GetCustomerStatus(int customerId)
        {
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                var customerExists = DAL.GetCustomerStatus(customerId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = customerExists;
                _result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse GetCustomer_Others(int customerId)
        {
            try
            {
                var customerExists = DAL.GetCustomerOthers(customerId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = customerExists;
                _result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        [HttpPost]
        public Utility.CustomResponse AddOtherCustomers(CustomerOthersDto customerDto)
        {
            try
            {

                var customerId = DAL.AddOtherCustomers(customerDto);
                if (customerId > 0)
                {
                    var customerInfo = DAL.GetCustomerOthers_V1(customerId);
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = customerInfo;
                    _result.Message = CustomConstants.Data_Saved_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
            }
            return _result;
        }
    }
}
