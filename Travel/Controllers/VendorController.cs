﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;
using Travel.Models;
using System.Web.Security;
using WebMatrix.WebData;
using System.Data.SqlClient;
using Microsoft.WindowsAzure.Storage;

namespace Travel.Controllers
{
    public class VendorController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetAllVendors()
        {
            try
            {

                List<VendorDTO> Vendors = DAL.GetAllVendors();
                if (Vendors.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Vendors;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse GetVendor(int VendorId)
        {
            try
            {

                VendorDTO Vendor = DAL.GetVendor(VendorId);
                if (Vendor.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Vendor;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddVendor(VendorDTO vendorDTO) //Merchant
        {
            try
            {
                vendorDTO.Password = "test";
                if (vendorDTO.Id == 0)
                {
                    WebSecurity.CreateUserAndAccount(vendorDTO.Email, vendorDTO.Password);
                }
                if (!vendorDTO.Base64String.StartsWith("http"))
                {
                    vendorDTO.Base64String = GetImageURL(vendorDTO.Base64String, vendorDTO.Format);
                }
                int vendorId = DAL.AddVendor(vendorDTO);
                if (vendorId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (vendorId > 0)
                {
                    VendorPaymentDTO vendorPayment = new VendorPaymentDTO();
                    vendorPayment.AccountNumber = vendorDTO.AccountNumber;
                    vendorPayment.BankName = vendorDTO.BankName;
                    vendorPayment.BranchName = vendorDTO.BranchName;
                    vendorPayment.IFSC = vendorDTO.IFSC;
                    vendorPayment.CreatedBy = vendorDTO.CreatedBy;
                    vendorPayment.VendorId = vendorId;
                    vendorPayment.Id = 0;
                    int vendorPaymentId = DAL.AddVendorPaymentDetails(vendorPayment);

                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorId;
                    Result.Message = CustomConstants.Data_Saved_Successfully;
                    SendMailForRegistration(vendorDTO.Email, vendorDTO.Password);
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddVendorUser(VendorUserDTO vendorUserDTO) //Agents
        {
            try
            {
                string password = "test";
                if (vendorUserDTO.Id == 0)
                {
                    WebSecurity.CreateUserAndAccount(vendorUserDTO.Email, password);
                }
                int vendorUserId = DAL.AddVendorUser(vendorUserDTO);
                if (vendorUserId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (vendorUserId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorUserId;
                    Result.Message = CustomConstants.Data_Saved_Successfully;
                    SendMailForRegistration(vendorUserDTO.Email, password);
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddVendorDeals(VendorDealsDTO VendorDealsDTO)
        {
            try
            {
                int vendorDealId = DAL.AddVendorDeals(VendorDealsDTO);
                if (vendorDealId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (vendorDealId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorDealId;
                    Result.Message = CustomConstants.Data_Saved_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddVendorRoute(VendorRouteDTO vendorRouteDTO)
        {
            try
            {

                int vendorRouteId = DAL.AddVendorRoute(vendorRouteDTO);
                if (vendorRouteId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else
                    if (vendorRouteId > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = vendorRouteId;
                        Result.Message = CustomConstants.Data_Saved_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse Login(string username, string password, int type)
        {
            try
            {

                bool isLogin = WebSecurity.Login(username, password, persistCookie: true);
                if (isLogin)
                {
                    if (type == 1 || type == 3)
                    {
                        VendorUserDTO vendor = DAL.GetVendorUser(username, type);
                        if (vendor.Id > 0)
                        {
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = vendor;
                            Result.Message = CustomConstants.Log_In_Successful;
                        }
                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.User_Does_Not_Exist;
                        }
                    }
                    else
                    {
                        VendorDTO vendor = DAL.GetMerchant(username);
                        if (vendor.Id > 0)
                        {
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = vendor;
                            Result.Message = CustomConstants.Log_In_Successful;
                        }
                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.User_Does_Not_Exist;
                        }
                    }

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.InValid_Credential;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string email)
        {

            try
            {
                string token = WebSecurity.GeneratePasswordResetToken(email);
                int isSent = SendMail(email, token);
                // string confirmPassword = "123456";
                //bool isReset = ResetPassword(token, confirmPassword);
                if (isSent == 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.MailSent;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Email_Not_sent;
                }
                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

        }

        public static int SendMail(string username, string token)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                //  myString = myString.Replace("$$UserEmail$$", email);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Forgot Password";
                Msg.Body = "hi";// myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }


        public static int SendMailForRegistration(string username, string password)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/VendorRegistration.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Ticktapp Registration";
                Msg.Body = myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string token, string confirmPassword)
        {
            bool isReset = false;
            try
            {
                isReset = WebSecurity.ResetPassword(token, confirmPassword);
                if (isReset)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Email_Not_sent;
                }


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
            return Result;
        }

        [HttpPut]
        public Utility.CustomResponse ChangePassword(string name, string oldpassword, string newpassword)
        {
            Utility.CustomResponse Result1 = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.ChangePassword(name, oldpassword, newpassword))
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.UpdateUser_Update_Successfully;
                }
                else
                {
                    Result1.Status = Utility.CustomResponseStatus.Successful;
                    Result1.Message = CustomConstants.Password_MisMatch;
                }

            }
            catch (Exception ex)
            {
                Result1.Status = Utility.CustomResponseStatus.Exception;
                Result1.Message = ex.Message;
            }
            return Result1;
        }

        [HttpGet]
        public Utility.CustomResponse GetVendorRoutesandBuses(int VendorId)
        {
            try
            {

                List<VendorRouteDTO> vendorRouteDTO = DAL.GetVendorRoutes(VendorId);
                if (vendorRouteDTO.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorRouteDTO;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetVendorDeals(int ServiceId, DateTime Date)
        {
            try
            {

                List<VendorDealsDTO> vendorDealsDTO = DAL.GetVendorDeals(ServiceId, Date);
                if (vendorDealsDTO.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorDealsDTO;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse GetClosedBids(int MerchantId)
        {
            try
            {

                List<BidDTO> Bids = DAL.GetClosedBids(MerchantId);
                if (Bids.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Bids;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse BidsByStatus(int MerchantId, int Status, int Offset, int Max)
        {
            try
            {
                DAL.CheckForCompletedBids();
                List<BidDTO> Bids = DAL.GetBidsByStatus(MerchantId, Status, Offset, Max);
                if (Bids.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Bids;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse AcceptBid(int BidId, int AcceptedBy, int CreatedBy)
        {
            try
            {
                int serviceDaysCount = DAL.CheckServiceDayExists(BidId);
                if (serviceDaysCount > 0)
                {
                    int bidId = DAL.AcceptBid(BidId, AcceptedBy, CreatedBy);
                    if (bidId > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = bidId;
                        Result.Message = CustomConstants.Data_Saved_Successfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecords_On_JourneyDate;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AllocateSeatNo(MatchingBidsDTO matchingBidsDTO)
        {
            try
            {

                int matchingBid = DAL.AddMatchingBids(matchingBidsDTO);
                if (matchingBid == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Record_Already_Exists;
                    Result.Response = 0;
                }
                else
                {
                    string[] strSeatNo = matchingBidsDTO.SeatNos.Split(',');
                    for (int i = 0; i < strSeatNo.Length; i++)
                    {
                        List<SeatDTO> seatInfo = DAL.AllocateSeatNo(matchingBidsDTO.BidId, strSeatNo[i], matchingBidsDTO.CreatedBy);
                        if (seatInfo.Count > 0)
                        {
                            //Send Customer Name from Bid -- Have to do
                            //DAL.PushNotification("Your Seat Nos are :" + matchingBidsDTO.SeatNos, seatInfo[0].CustomerName);
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = 0;// matchingBidsDTO.SeatNos;
                            Result.Message = CustomConstants.Data_Saved_Successfully;
                        }
                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.AccessDenied;
                            Result.Response = 0;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddException(ExceptionDTO exceptionDTO)
        {
            try
            {

                int exceptionId = DAL.AddException(exceptionDTO);
                if (exceptionId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = exceptionId;
                    Result.Message = CustomConstants.Data_Saved_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse GetExceptions(int Userid)
        {
            try
            {

                List<ExceptionDTO> exception = DAL.GetExceptions(Userid);
                if (exception.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = exception;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetUserType(int userid)
        {
            try
            {
                int usertype = DAL.GetUserType(userid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = usertype;
                Result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteVendor(int VendorId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                VendorDTO vendor = DAL.GetVendor(VendorId);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(vendor.Email); // deletes record from webpages_Membership table
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(vendor.Email, true); // deletes record from UserProfile table

                var merchantId = DAL.DeleteMerchant(VendorId);
                if (merchantId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = merchantId;
                    Result.Message = CustomConstants.Merchant_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetServiceDayForService(int ServiceId, DateTime Date)
        {
            try
            {

                int ServiceDayId = DAL.GetServiceDayForService(ServiceId, Date);
                if (ServiceDayId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = ServiceDayId;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;


        }


        [HttpGet]
        public Utility.CustomResponse GetServicesForRoute(int BidId, int MerchantId)
        {
            try
            {

                List<BidDTO> bids = DAL.GetServicesForRoute(BidId, MerchantId);
                if (bids.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = bids;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPut]
        public Utility.CustomResponse ChangeBidStatus(UpdateBidsDTO updateBidsDTO)
        {
            try
            {
                for (int i = 0; i < updateBidsDTO.BidIds.Count; i++)
                {
                    int Id = DAL.UpdateBidStatus(updateBidsDTO.BidIds[i], updateBidsDTO.status, updateBidsDTO.ChangedBy);
                }
                Result.Response = updateBidsDTO.BidIds;
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.Data_Saved_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse GetVendorUsers(int VendorId)
        {
            try
            {

                List<VendorUserDTO> VendorUsers = DAL.GetVendorUsersByVendor(VendorId);
                if (VendorUsers.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = VendorUsers;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteVendorUser(int VendorUserID)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                VendorUserDTO vendor = DAL.GetVendorUserByID(VendorUserID);
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(vendor.Email); // deletes record from webpages_Membership table
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(vendor.Email, true); // deletes record from UserProfile table
                var merchantId = DAL.DeleteAgent(VendorUserID);
                if (merchantId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = merchantId;
                    Result.Message = CustomConstants.Agent_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetAllDealsByVendorId(int VendorId)
        {
            try
            {

                List<VendorDealsDTO> vendorDealsDTO = DAL.GetAllDealsByVendorId(VendorId);
                if (vendorDealsDTO.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorDealsDTO;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetDealsById(int VendorDealId)
        {
            try
            {

                VendorDealsDTO vendorDealsDTO = DAL.GetDealsById(VendorDealId);
                if (vendorDealsDTO.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorDealsDTO;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteVendorDeal(int VendorDealId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                var vendorDealId = DAL.DeleteVendorDeal(VendorDealId);
                if (vendorDealId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = vendorDealId;
                    Result.Message = CustomConstants.VendorDeal_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetBidStatistics(int UserId)
        {
            try
            {

                BidStatisticsDTO BidStatistics = DAL.GetBidStatistics(UserId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = BidStatistics;
                Result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetVendorByBidId(int BidId)
        {
            try
            {

                List<VendorDTO> Vendor = DAL.GetVendorByBidId(BidId);
                if (Vendor.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Vendor;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        private string GetImageURL(string binary, string SermonFormat)
        {
            Random r = new Random();
            string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + "." + SermonFormat;
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccountvideo = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            // Create the blob client.
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo = storageAccountvideo.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo = blobClientvideo.GetContainerReference("mycontainer");

            // Create the container if it doesn't already exist.
            containervideo.CreateIfNotExists();


            containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
            {
                PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
            });


            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo = containervideo.GetBlockBlobReference(filename);

            // Create or overwrite the "myblob" blob with contents from a local file.

            byte[] binarydatavideo = Convert.FromBase64String(binary);

            //using (var fileStream = binarydata)
            //{
            blockBlobvideo.Properties.ContentType = "video/" + SermonFormat;
            blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl = containervideo.GetBlockBlobReference(filename);

            string imageurlvideo = blockBlobvideourl.Uri.ToString();

            return imageurlvideo;
        }


    }
}

