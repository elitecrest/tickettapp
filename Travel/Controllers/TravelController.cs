﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Travel.Models;

namespace Travel.Controllers
{
    public class TravelController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Cities()
        {
            try
            {

                List<CityDTO> cities = DAL.GetCities();
                if (cities.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = cities;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse Amenities()
        {
            try
            {

                List<AmenitiesDTO> Amenities = DAL.GetAmenities();
                if (Amenities.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Amenities;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse BusTypes()
        {
            try
            {

                List<BusTypesDTO> BusTypes = DAL.GetBusTypes();
                if (BusTypes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = BusTypes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse PaymentTypes()
        {
            try
            {

                List<PaymentTypesDTO> PaymentTypes = DAL.GetPaymentTypes();
                if (PaymentTypes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PaymentTypes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddLocation(LocationDTO locationDTO)
        {
            try
            {

                int LocationId = DAL.AddLocation(locationDTO);
                if (LocationId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (LocationId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = LocationId;
                    Result.Message = CustomConstants.Location_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse DayTypes()
        {
            try
            {

                List<DayTypesDTO> DayTypes = DAL.GetDayTypes();
                if (DayTypes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = DayTypes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetLocationsByCityId(int cityId)
        {
            try
            {

                List<LocationDTO> locationsList = DAL.GetLocationsByCityId(cityId);


                if (locationsList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = locationsList;
                    Result.Message = CustomConstants.Location_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddBusType(BusTypesDTO busTypesDTO)
        {
            try
            {

                int BusTypeId = DAL.AddBusType(busTypesDTO);
                if (BusTypeId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (BusTypeId > 0)
                {
                    if (busTypesDTO.Amenities != null && busTypesDTO.Amenities.Count > 0)
                    {
                        foreach (var t in busTypesDTO.Amenities)
                        {
                            DAL.AddBusAmenities(BusTypeId, t);
                        }
                    }
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = BusTypeId;
                    Result.Message = CustomConstants.BusType_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpPost]
        public Utility.CustomResponse AddCity(CityDTO cityDTO)
        {
            try
            {

                int CityId = DAL.AddCity(cityDTO);
                if (CityId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (CityId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = CityId;
                    Result.Message = CustomConstants.City_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteBusType(int Id)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                var busTypeId = DAL.DeleteBusType(Id);
                if (busTypeId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = busTypeId;
                    Result.Message = CustomConstants.BusType_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddRoute(RouteDTO routeDTO)
        {
            try
            {

                int routeId = DAL.AddRoute(routeDTO);
                if (routeId == -99)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.Already_Exists;
                }
                else if (routeId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = routeId;
                    Result.Message = CustomConstants.Route_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetRoutes(int RouteId)
        {
            try
            {

                List<RouteDTO> routes = DAL.GetRoutes(RouteId);


                if (routes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = routes;
                    Result.Message = CustomConstants.Location_Added_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteRoute(int RouteId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                var routeId = DAL.DeleteRoute(RouteId);
                if (routeId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = routeId;
                    Result.Message = CustomConstants.Route_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetRouteLocations(int RouteId)
        {
            try
            {

                RouteLocationsDTO routes = DAL.GetRouteLocations(RouteId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = routes;
                Result.Message = CustomConstants.Details_Get_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteCity(int CityId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                var cityId = DAL.DeleteCity(CityId);
                if (cityId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = cityId;
                    Result.Message = CustomConstants.City_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteLocation(int LocationId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                var locationId = DAL.DeleteLocation(LocationId);
                if (locationId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = locationId;
                    Result.Message = CustomConstants.Location_Deleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetCancelReasons()
        {
            try
            {

                List<CancelReasonsDTO> reasons = DAL.GetCancelReasons();


                if (reasons.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = reasons;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetBusTypeById(int busTypeId)
        {
            try
            {

                BusTypesDTO busTypes = DAL.GetBusTypeById(busTypeId);
                if (busTypes != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = busTypes;
                    Result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
