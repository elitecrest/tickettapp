﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Microsoft.WindowsAzure.Storage;

namespace Travel.Models
{
    public class DAL
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["TravelDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static List<CityDTO> GetCities()
        {
            List<CityDTO> cities = new List<CityDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CityDTO v = new CityDTO();
                        v.Id = (int)myData["city_id"];
                        v.Name = (string)myData["city_name"].ToString();
                        v.State = (string)myData["city_state"].ToString();
                        v.Zip = (myData["zip"] == DBNull.Value) ? "" : (string)myData["zip"].ToString();
                        cities.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cities;
        }





        internal static List<AmenitiesDTO> GetAmenities()
        {
            List<AmenitiesDTO> Amenities = new List<AmenitiesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AmenitiesDTO v = new AmenitiesDTO();
                        v.Id = (int)myData["Id"];
                        v.Name = (String)myData["name"].ToString();
                        v.Cost = (Decimal)myData["Cost"];

                        Amenities.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Amenities;
        }



        internal static List<BusTypesDTO> GetBusTypes()
        {
            List<BusTypesDTO> BusTypes = new List<BusTypesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBusTypes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        BusTypesDTO v = new BusTypesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["BusType"].ToString();
                       // v.IsUsed = Convert.ToInt32(myData["IsUsed"]);
                        v.CategoryId = (myData["CategoryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CategoryId"]);
                        BusTypes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return BusTypes;
        }


        internal static List<PaymentTypesDTO> GetPaymentTypes()
        {
            List<PaymentTypesDTO> PaymentTypes = new List<PaymentTypesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPaymentTypes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PaymentTypesDTO v = new PaymentTypesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        PaymentTypes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return PaymentTypes;
        }



        internal static List<CustomerDTO> GetAllCustomers()
        {
            List<CustomerDTO> Customer = new List<CustomerDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCustomer]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter CustomerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    CustomerId.Direction = System.Data.ParameterDirection.Input;
                    CustomerId.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CustomerDTO v = new CustomerDTO();
                        v.Id = (int)myData["id"];
                        v.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"].ToString();
                        v.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"].ToString();
                        v.DOB = (myData["DOB"] == DBNull.Value) ? "" : (string)myData["DOB"].ToString();
                        v.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"].ToString();
                        v.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.CityId = (myData["CityId"] == DBNull.Value) ? 0 : (int)myData["CityId"];
                        Customer.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Customer;
        }


        internal static CustomerDTO GetCustomer(int customerId)
        {
            CustomerDTO Customer = new CustomerDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCustomer]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter CustomerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    CustomerId.Direction = System.Data.ParameterDirection.Input;
                    CustomerId.Value = customerId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Customer.Id = (int)myData["id"];
                        Customer.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"].ToString();
                        Customer.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"].ToString();
                        Customer.DOB = (myData["DOB"] == DBNull.Value) ? "" : (string)myData["DOB"].ToString();
                        Customer.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"].ToString();
                        Customer.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"].ToString();
                        Customer.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        Customer.CityId = (myData["CityId"] == DBNull.Value) ? 0 : (int)myData["CityId"];
                        //Customer.CityName = (myData["CityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CityName"]);
                        Customer.Password = myData["Password"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Password"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Customer;
        }

        internal static int AddCustomer(CustomerDTO customerDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCustomer]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter FirstName = cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar, 50);
                    FirstName.Direction = System.Data.ParameterDirection.Input;
                    FirstName.Value = customerDTO.FirstName;

                    SqlParameter LastName = cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar, 50);
                    LastName.Direction = System.Data.ParameterDirection.Input;
                    LastName.Value = customerDTO.LastName;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", System.Data.SqlDbType.VarChar, 50);
                    DOB.Direction = System.Data.ParameterDirection.Input;
                    DOB.Value = customerDTO.DOB;

                    SqlParameter Email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    Email.Direction = System.Data.ParameterDirection.Input;
                    Email.Value = customerDTO.Email;

                    SqlParameter Phone = cmd.Parameters.Add("@Phone", System.Data.SqlDbType.VarChar, 20);
                    Phone.Direction = System.Data.ParameterDirection.Input;
                    Phone.Value = customerDTO.Phone;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", System.Data.SqlDbType.Int);
                    Gender.Direction = System.Data.ParameterDirection.Input;
                    Gender.Value = customerDTO.Gender;

                    SqlParameter CityId = cmd.Parameters.Add("@CityId", System.Data.SqlDbType.Int);
                    CityId.Direction = System.Data.ParameterDirection.Input;
                    CityId.Value = customerDTO.CityId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = customerDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = customerDTO.Type;

                    SqlParameter password = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 50);
                    password.Direction = System.Data.ParameterDirection.Input;
                    password.Value = customerDTO.Password;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = customerDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = customerDTO.Id;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static LocationDTO GetLocation()
        {
            LocationDTO Location = new LocationDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetLocation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Location.Id = (int)myData["id"];
                        Location.Name = (string)myData["Name"].ToString();
                        Location.Address = (string)myData["Address"].ToString();
                        Location.CityName = (string)myData["city_name"].ToString();
                        Location.CityState = (string)myData["city_state"].ToString();
                        Location.CityZip = (string)myData["zip"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Location;
        }


        internal static List<VendorDTO> GetAllVendors()
        {
            List<VendorDTO> Vendor = new List<VendorDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendor]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        VendorDTO v = new VendorDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.ContactName = (string)myData["Contact_Name"].ToString();
                        v.Address1 = (string)myData["Address1"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Address2 = (string)myData["Address2"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.Phone1 = (string)myData["Phone1"].ToString();
                        v.Phone2 = (string)myData["Phone2"].ToString();
                        v.Fax = (string)myData["Fax"].ToString();
                        v.Base64String = (myData["Logo"] == DBNull.Value) ? string.Empty : (string)myData["Logo"].ToString();
                        v.BankName = (string)myData["BankName"].ToString();
                        v.BranchName = (string)myData["BranchName"].ToString();
                        v.AccountNumber = (string)myData["AccountNumber"].ToString();
                        v.IFSC = (string)myData["IFSC"].ToString();
                        Vendor.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Vendor;
        }


        internal static VendorDTO GetVendor(int vendorId)
        {
            VendorDTO Vendor = new VendorDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendor]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = vendorId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Vendor.Id = (int)myData["id"];
                        Vendor.Name = (string)myData["Name"].ToString();
                        Vendor.ContactName = (string)myData["Contact_Name"].ToString();
                        Vendor.Address1 = (string)myData["Address1"].ToString();
                        Vendor.Email = (string)myData["Email"].ToString();
                        Vendor.Address2 = (string)myData["Address2"].ToString();
                        Vendor.City = (string)myData["City"].ToString();
                        Vendor.State = (string)myData["State"].ToString();
                        Vendor.Zip = (string)myData["Zip"].ToString();
                        Vendor.Phone1 = (string)myData["Phone1"].ToString();
                        Vendor.Phone2 = (string)myData["Phone2"].ToString();
                        Vendor.Fax = (string)myData["Fax"].ToString();
                        Vendor.Base64String = (myData["Logo"] == DBNull.Value) ? string.Empty : (string)myData["Logo"].ToString();
                        Vendor.BankName = (string)myData["BankName"].ToString();
                        Vendor.BranchName = (string)myData["BranchName"].ToString();
                        Vendor.AccountNumber = (string)myData["AccountNumber"].ToString();
                        Vendor.IFSC = (string)myData["IFSC"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Vendor;
        }

        internal static int AddVendor(VendorDTO vendorDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVendor]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 50);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = vendorDTO.Name;

                    SqlParameter Contact_Name = cmd.Parameters.Add("@Contact_Name", System.Data.SqlDbType.VarChar, 50);
                    Contact_Name.Direction = System.Data.ParameterDirection.Input;
                    Contact_Name.Value = vendorDTO.ContactName;

                    SqlParameter Phone1 = cmd.Parameters.Add("@Phone1", System.Data.SqlDbType.VarChar, 20);
                    Phone1.Direction = System.Data.ParameterDirection.Input;
                    Phone1.Value = vendorDTO.Phone1;

                    SqlParameter Phone2 = cmd.Parameters.Add("@Phone2", System.Data.SqlDbType.VarChar, 20);
                    Phone2.Direction = System.Data.ParameterDirection.Input;
                    Phone2.Value = vendorDTO.Phone2;

                    SqlParameter Email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 50);
                    Email.Direction = System.Data.ParameterDirection.Input;
                    Email.Value = vendorDTO.Email;

                    SqlParameter Address1 = cmd.Parameters.Add("@Address1", System.Data.SqlDbType.VarChar, 50);
                    Address1.Direction = System.Data.ParameterDirection.Input;
                    Address1.Value = vendorDTO.Address1;

                    SqlParameter Address2 = cmd.Parameters.Add("@Address2", System.Data.SqlDbType.VarChar, 50);
                    Address2.Direction = System.Data.ParameterDirection.Input;
                    Address2.Value = vendorDTO.Address2;

                    SqlParameter City = cmd.Parameters.Add("@City", System.Data.SqlDbType.VarChar, 50);
                    City.Direction = System.Data.ParameterDirection.Input;
                    City.Value = vendorDTO.City;

                    SqlParameter State = cmd.Parameters.Add("@State", System.Data.SqlDbType.VarChar, 50);
                    State.Direction = System.Data.ParameterDirection.Input;
                    State.Value = vendorDTO.State;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", System.Data.SqlDbType.VarChar, 20);
                    Zip.Direction = System.Data.ParameterDirection.Input;
                    Zip.Value = vendorDTO.Zip;

                    SqlParameter Fax = cmd.Parameters.Add("@Fax", System.Data.SqlDbType.VarChar, 20);
                    Fax.Direction = System.Data.ParameterDirection.Input;
                    Fax.Value = vendorDTO.Fax;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = vendorDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = vendorDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = vendorDTO.CreatedBy;

                    SqlParameter Logo = cmd.Parameters.Add("@Logo", System.Data.SqlDbType.VarChar, 4000);
                    Logo.Direction = System.Data.ParameterDirection.Input;
                    Logo.Value = vendorDTO.Base64String;

                    SqlParameter Branch = cmd.Parameters.Add("@BranchName", System.Data.SqlDbType.VarChar, 50);
                    Branch.Direction = System.Data.ParameterDirection.Input;
                    Branch.Value = vendorDTO.BranchName;

                    SqlParameter Bank = cmd.Parameters.Add("@BankName", System.Data.SqlDbType.VarChar, 50);
                    Bank.Direction = System.Data.ParameterDirection.Input;
                    Bank.Value = vendorDTO.BankName;
                    
                    SqlParameter AccountNum = cmd.Parameters.Add("@AccountNumber", System.Data.SqlDbType.VarChar, 50);
                    AccountNum.Direction = System.Data.ParameterDirection.Input;
                    AccountNum.Value = vendorDTO.AccountNumber;

                    SqlParameter IFSCCode = cmd.Parameters.Add("@IFSC", System.Data.SqlDbType.VarChar, 50);
                    IFSCCode.Direction = System.Data.ParameterDirection.Input;
                    IFSCCode.Value = vendorDTO.IFSC;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = vendorDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }




        internal static int AddVendorUser(VendorUserDTO vendorUserDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVendorUser]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //SqlParameter UserId = cmd.Parameters.Add("@UserId", System.Data.SqlDbType.Int);
                    //UserId.Direction = System.Data.ParameterDirection.Input;
                    //UserId.Value = vendorUserDTO.Id;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 50);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = vendorUserDTO.Name;

                    SqlParameter Phone = cmd.Parameters.Add("@Phone", System.Data.SqlDbType.VarChar, 20);
                    Phone.Direction = System.Data.ParameterDirection.Input;
                    Phone.Value = vendorUserDTO.Phone;

                    SqlParameter Email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 50);
                    Email.Direction = System.Data.ParameterDirection.Input;
                    Email.Value = vendorUserDTO.Email;

                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = vendorUserDTO.VendorId;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", System.Data.SqlDbType.Int);
                    UserType.Direction = System.Data.ParameterDirection.Input;
                    UserType.Value = vendorUserDTO.UserType;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = vendorUserDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = vendorUserDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = vendorUserDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = vendorUserDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int AddBus(BusDTO busDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter NoOfSeats = cmd.Parameters.Add("@NoOfSeats", System.Data.SqlDbType.Int);
                    NoOfSeats.Direction = System.Data.ParameterDirection.Input;
                    NoOfSeats.Value = busDTO.NoOfSeats;

                    SqlParameter IsAc = cmd.Parameters.Add("@IsAc", System.Data.SqlDbType.Int);
                    IsAc.Direction = System.Data.ParameterDirection.Input;
                    IsAc.Value = busDTO.IsAc;

                    SqlParameter SKU = cmd.Parameters.Add("@SKU", System.Data.SqlDbType.VarChar, 50);
                    SKU.Direction = System.Data.ParameterDirection.Input;
                    SKU.Value = busDTO.SKU;

                    SqlParameter BusTypeId = cmd.Parameters.Add("@BusTypeId", System.Data.SqlDbType.Int);
                    BusTypeId.Direction = System.Data.ParameterDirection.Input;
                    BusTypeId.Value = busDTO.BusTypeId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = busDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = busDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = busDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = busDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int AddBid(BidDTO bidDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CustomerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    CustomerId.Direction = System.Data.ParameterDirection.Input;
                    CustomerId.Value = bidDTO.CustomerId;

                    SqlParameter ServicedayId = cmd.Parameters.Add("@ServiceDayId", System.Data.SqlDbType.Int);
                    ServicedayId.Direction = System.Data.ParameterDirection.Input;
                    ServicedayId.Value = bidDTO.ServiceDayId;

                    SqlParameter JourneyDate = cmd.Parameters.Add("@JourneyDate", System.Data.SqlDbType.DateTime);
                    JourneyDate.Direction = System.Data.ParameterDirection.Input;
                    JourneyDate.Value = bidDTO.JourneyDate;

                    SqlParameter Amount = cmd.Parameters.Add("@Amount", System.Data.SqlDbType.Decimal);
                    Amount.Direction = System.Data.ParameterDirection.Input;
                    Amount.Value = bidDTO.Amount;

                    SqlParameter BidStatus = cmd.Parameters.Add("@BidStatus", System.Data.SqlDbType.Int);
                    BidStatus.Direction = System.Data.ParameterDirection.Input;
                    BidStatus.Value = bidDTO.BidStatus;

                    SqlParameter NoOfSeats = cmd.Parameters.Add("@NoOfSeats", System.Data.SqlDbType.Int);
                    NoOfSeats.Direction = System.Data.ParameterDirection.Input;
                    NoOfSeats.Value = bidDTO.NoOfSeats;

                    SqlParameter Self = cmd.Parameters.Add("@Self", System.Data.SqlDbType.Int);
                    Self.Direction = System.Data.ParameterDirection.Input;
                    Self.Value = bidDTO.Self;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = bidDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = bidDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = bidDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.Input;
                    Id.Value = bidDTO.Id;

                    SqlParameter fromCityId = cmd.Parameters.Add("@FromCityId", System.Data.SqlDbType.Int);
                    fromCityId.Direction = System.Data.ParameterDirection.Input;
                    fromCityId.Value = bidDTO.fromCityId;

                    SqlParameter toCityId = cmd.Parameters.Add("@ToCityId", System.Data.SqlDbType.Int);
                    toCityId.Direction = System.Data.ParameterDirection.Input;
                    toCityId.Value = bidDTO.toCityId;

                    SqlParameter contactNumber = cmd.Parameters.Add("@PhoneNumber", System.Data.SqlDbType.VarChar, 20);
                    contactNumber.Direction = System.Data.ParameterDirection.Input;
                    contactNumber.Value = bidDTO.PhoneNumber;

                    SqlParameter timeSlot = cmd.Parameters.Add("@TimeSlot", System.Data.SqlDbType.Int);
                    timeSlot.Direction = System.Data.ParameterDirection.Input;
                    timeSlot.Value = Convert.ToInt32(bidDTO.TimeSlotType);

                    SqlParameter busCategory = cmd.Parameters.Add("@BusTypeID", System.Data.SqlDbType.Int);
                    busCategory.Direction = System.Data.ParameterDirection.Input;
                    busCategory.Value = Convert.ToInt32(bidDTO.BusCategoryName);

                    SqlParameter customerName = cmd.Parameters.Add("@CustomerName", System.Data.SqlDbType.VarChar, 50);
                    customerName.Direction = System.Data.ParameterDirection.Input;
                    customerName.Value = bidDTO.CustomerName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["id"];
                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddLocation(LocationDTO locationDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddLocation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.NVarChar, 100);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = locationDTO.Name;

                    SqlParameter Address = cmd.Parameters.Add("@Address", System.Data.SqlDbType.NVarChar, 50);
                    Address.Direction = System.Data.ParameterDirection.Input;
                    Address.Value = locationDTO.Address;

                    SqlParameter City_Id = cmd.Parameters.Add("@City_Id", System.Data.SqlDbType.Int);
                    City_Id.Direction = System.Data.ParameterDirection.Input;
                    City_Id.Value = locationDTO.CityId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = locationDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = locationDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = locationDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = locationDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddService(ServiceDTO serviceDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddService]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter RouteId = cmd.Parameters.Add("@RouteId", System.Data.SqlDbType.Int);
                    RouteId.Direction = System.Data.ParameterDirection.Input;
                    RouteId.Value = serviceDTO.RouteId;

                    SqlParameter ServiceNumber = cmd.Parameters.Add("@ServiceNumber", System.Data.SqlDbType.Int);
                    ServiceNumber.Direction = System.Data.ParameterDirection.Input;
                    ServiceNumber.Value = serviceDTO.ServiceNumber;

                    SqlParameter FromTime = cmd.Parameters.Add("@FromTime", System.Data.SqlDbType.VarChar, 20);
                    FromTime.Direction = System.Data.ParameterDirection.Input;
                    FromTime.Value = serviceDTO.FromTime;

                    SqlParameter ToTime = cmd.Parameters.Add("@ToTime", System.Data.SqlDbType.VarChar, 20);
                    ToTime.Direction = System.Data.ParameterDirection.Input;
                    ToTime.Value = serviceDTO.ToTime;

                    SqlParameter NoOfStops = cmd.Parameters.Add("@NoOfStops", System.Data.SqlDbType.Int);
                    NoOfStops.Direction = System.Data.ParameterDirection.Input;
                    NoOfStops.Value = serviceDTO.NoOfStops;

                    SqlParameter DayTypeId = cmd.Parameters.Add("@DayTypeId", System.Data.SqlDbType.Int);
                    DayTypeId.Direction = System.Data.ParameterDirection.Input;
                    DayTypeId.Value = serviceDTO.DayTypeId;


                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = serviceDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = serviceDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = serviceDTO.CreatedBy;

                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = serviceDTO.VendorId;

                    SqlParameter BusId = cmd.Parameters.Add("@BusId", System.Data.SqlDbType.Int);
                    BusId.Direction = System.Data.ParameterDirection.Input;
                    BusId.Value = serviceDTO.BusTypeId;

                    SqlParameter StartLocation = cmd.Parameters.Add("@StartLocation", System.Data.SqlDbType.Int);
                    StartLocation.Direction = System.Data.ParameterDirection.Input;
                    StartLocation.Value = serviceDTO.StartLocation;

                    SqlParameter EndLocation = cmd.Parameters.Add("@EndLocation", System.Data.SqlDbType.Int);
                    EndLocation.Direction = System.Data.ParameterDirection.Input;
                    EndLocation.Value = serviceDTO.EndLocation;

                    SqlParameter RetailPrice = cmd.Parameters.Add("@RetailPrice", System.Data.SqlDbType.Decimal);
                    RetailPrice.Direction = System.Data.ParameterDirection.Input;
                    RetailPrice.Value = serviceDTO.RetailPrice;

                    SqlParameter SpecialPrice = cmd.Parameters.Add("@SpecialPrice", System.Data.SqlDbType.Decimal);
                    SpecialPrice.Direction = System.Data.ParameterDirection.Input;
                    SpecialPrice.Value = serviceDTO.SpecialPrice;

                    SqlParameter ServiceFromDate = cmd.Parameters.Add("@ServiceFromDate", System.Data.SqlDbType.DateTime);
                    ServiceFromDate.Direction = System.Data.ParameterDirection.Input;
                    ServiceFromDate.Value = serviceDTO.FromDate;

                    SqlParameter ServiceToDate = cmd.Parameters.Add("@ServiceToDate", System.Data.SqlDbType.DateTime);
                    ServiceToDate.Direction = System.Data.ParameterDirection.Input;
                    ServiceToDate.Value = serviceDTO.ToDate;


                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = serviceDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddServiceDays(ServiceDaysDTO serviceDaysDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddServiceDays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceDaysDTO.ServiceId;

                    SqlParameter Date = cmd.Parameters.Add("@Date", System.Data.SqlDbType.DateTime);
                    Date.Direction = System.Data.ParameterDirection.Input;
                    Date.Value = serviceDaysDTO.Date;

                    SqlParameter Time = cmd.Parameters.Add("@Time", System.Data.SqlDbType.VarChar, 20);
                    Time.Direction = System.Data.ParameterDirection.Input;
                    Time.Value = serviceDaysDTO.Time;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = serviceDaysDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddServiceAmenities(ServiceAmenitiesDTO serviceAmenitiesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddServiceAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceAmenitiesDTO.ServiceId;

                    SqlParameter AmenityId = cmd.Parameters.Add("@AmenityId", System.Data.SqlDbType.Int);
                    AmenityId.Direction = System.Data.ParameterDirection.Input;
                    AmenityId.Value = serviceAmenitiesDTO.AmenityId;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = serviceAmenitiesDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = serviceAmenitiesDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int AddServiceStops(ServiceStopsDTO serviceStopsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddServiceStops]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceStopsDTO.ServiceId;

                    SqlParameter LocationId = cmd.Parameters.Add("@LocationId", System.Data.SqlDbType.Int);
                    LocationId.Direction = System.Data.ParameterDirection.Input;
                    LocationId.Value = serviceStopsDTO.LocationId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = serviceStopsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int AddVendorDeals(VendorDealsDTO vendorDealsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVendorDeals]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = vendorDealsDTO.ServiceId;

                    SqlParameter Contact_Name = cmd.Parameters.Add("@Date", System.Data.SqlDbType.VarChar, 50);
                    Contact_Name.Direction = System.Data.ParameterDirection.Input;
                    Contact_Name.Value = vendorDealsDTO.Date;

                    SqlParameter DiscountPercentage = cmd.Parameters.Add("@DiscountPercentage", System.Data.SqlDbType.Int);
                    DiscountPercentage.Direction = System.Data.ParameterDirection.Input;
                    DiscountPercentage.Value = vendorDealsDTO.DiscountPercentage;

                    SqlParameter OfferedPrice = cmd.Parameters.Add("@OfferedPrice", System.Data.SqlDbType.VarChar, 20);
                    OfferedPrice.Direction = System.Data.ParameterDirection.Input;
                    OfferedPrice.Value = vendorDealsDTO.OfferedPrice;

                    SqlParameter StartDate = cmd.Parameters.Add("@StartDate", System.Data.SqlDbType.DateTime);
                    StartDate.Direction = System.Data.ParameterDirection.Input;
                    StartDate.Value = vendorDealsDTO.FromDate;

                    SqlParameter EndDate = cmd.Parameters.Add("@EndDate", System.Data.SqlDbType.DateTime);
                    EndDate.Direction = System.Data.ParameterDirection.Input;
                    EndDate.Value = vendorDealsDTO.ToDate;

                    SqlParameter AvailableSeats = cmd.Parameters.Add("@AvailableSeats", System.Data.SqlDbType.Int);
                    AvailableSeats.Direction = System.Data.ParameterDirection.Input;
                    AvailableSeats.Value = vendorDealsDTO.AvailableSeats;

                    SqlParameter RemainingSeats = cmd.Parameters.Add("@RemainingSeats", System.Data.SqlDbType.Int);
                    RemainingSeats.Direction = System.Data.ParameterDirection.Input;
                    RemainingSeats.Value = vendorDealsDTO.RemainingSeats;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = vendorDealsDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = vendorDealsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddVendorRoute(VendorRouteDTO vendorRouteDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVendorRoute]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter FromCity = cmd.Parameters.Add("@FromCity", System.Data.SqlDbType.Int);
                    FromCity.Direction = System.Data.ParameterDirection.Input;
                    FromCity.Value = vendorRouteDTO.FromCity;

                    SqlParameter ToCity = cmd.Parameters.Add("@ToCity", System.Data.SqlDbType.Int);
                    ToCity.Direction = System.Data.ParameterDirection.Input;
                    ToCity.Value = vendorRouteDTO.ToCity;

                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = vendorRouteDTO.VendorId;

                    SqlParameter RetailPrice = cmd.Parameters.Add("@RetailPrice", System.Data.SqlDbType.Decimal);
                    RetailPrice.Direction = System.Data.ParameterDirection.Input;
                    RetailPrice.Value = vendorRouteDTO.RetailPrice;

                    SqlParameter Distance = cmd.Parameters.Add("@Distance", System.Data.SqlDbType.Decimal);
                    Distance.Direction = System.Data.ParameterDirection.Input;
                    Distance.Value = vendorRouteDTO.Distance;

                    SqlParameter SpecialPrice = cmd.Parameters.Add("@SpecialPrice", System.Data.SqlDbType.Decimal);
                    SpecialPrice.Direction = System.Data.ParameterDirection.Input;
                    SpecialPrice.Value = vendorRouteDTO.SpecialPrice;

                    SqlParameter BusId = cmd.Parameters.Add("@BusId", System.Data.SqlDbType.Int);
                    BusId.Direction = System.Data.ParameterDirection.Input;
                    BusId.Value = vendorRouteDTO.BusId;

                    SqlParameter StartLocation = cmd.Parameters.Add("@StartLocation", System.Data.SqlDbType.Int);
                    StartLocation.Direction = System.Data.ParameterDirection.Input;
                    StartLocation.Value = vendorRouteDTO.StartLocation;

                    SqlParameter EndLocation = cmd.Parameters.Add("@EndLocation", System.Data.SqlDbType.Int);
                    EndLocation.Direction = System.Data.ParameterDirection.Input;
                    EndLocation.Value = vendorRouteDTO.EndLocation;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = vendorRouteDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = vendorRouteDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = 1;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = vendorRouteDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<BidDTO> GetBids(int CustomerId)
        {
            List<BidDTO> Bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter customerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    customerId.Direction = System.Data.ParameterDirection.Input;
                    customerId.Value = CustomerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidDTO v = new BidDTO();
                        ServiceDTO s = new ServiceDTO();
                        ServiceDaysDTO sd = new ServiceDaysDTO();
                        CustomerDTO customer = new CustomerDTO();
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        v.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["JourneyDate"];
                        v.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        v.Self = (myData["Self"] == DBNull.Value) ? true : (bool)myData["Self"];
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        v.ServiceDTO = s;
                        sd.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        v.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"];
                        v.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"];
                        v.ServiceDayDTO = sd;
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        customer.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        customer.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];
                        customer.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"];
                        customer.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"];
                        v.customerInfo = customer;
                        Bids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Bids;
        }


        internal static List<DayTypesDTO> GetDayTypes()
        {
            List<DayTypesDTO> DayTypes = new List<DayTypesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDayTypes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        DayTypesDTO v = new DayTypesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        DayTypes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return DayTypes;
        }


        internal static List<ServiceAmenitiesDTO> GetServiceAmenities(int serviceId)
        {
            List<ServiceAmenitiesDTO> ServiceAmenities = new List<ServiceAmenitiesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServiceAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ServiceAmenitiesDTO v = new ServiceAmenitiesDTO();
                        AmenitiesDTO a = new AmenitiesDTO();
                        ServiceDTO s = new ServiceDTO();
                        v.Id = (int)myData["id"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        s.NoOfStops = (myData["NoOfStops"] == DBNull.Value) ? 0 : (int)myData["NoOfStops"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        a.Name = (myData["AmenityName"] == DBNull.Value) ? "" : (string)myData["AmenityName"];
                        a.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (decimal)myData["Cost"];
                        v.AmenitiesDTO = a;
                        v.ServiceDTO = s;
                        ServiceAmenities.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ServiceAmenities;
        }


        internal static List<int> GetAmenitiesForService(int serviceId)
        {
            List<int> AmenitiesDTO = new List<int>();
            int Id = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServiceAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {


                        Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        //  a.Name = (myData["AmenityName"] == DBNull.Value) ? "" : (string)myData["AmenityName"];
                        //a.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (decimal)myData["Cost"];
                        AmenitiesDTO.Add(Id);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return AmenitiesDTO;
        }


        internal static List<int> GetLocationsForService(int serviceId)
        {
            List<int> LocationDTO = new List<int>();
            List<int> newLocationList = new List<int>();
            int Id = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServiceLocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Id = (myData["LocationId"] == DBNull.Value) ? 0 : (int)myData["LocationId"];

                        //  a.Name = (myData["name"] == DBNull.Value) ? "" : (string)myData["name"];
                        // a.Address = (myData["Address"] == DBNull.Value) ? "" : (string)myData["Address"];
                        LocationDTO.Add(Id);
                    }
                    newLocationList = LocationDTO.Distinct().ToList();
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return newLocationList;
        }

        internal static int AddTransaction(TransactionDTO transactionDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTransaction]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter PaymentTypeId = cmd.Parameters.Add("@PaymentTypeId", System.Data.SqlDbType.Int);
                    PaymentTypeId.Direction = System.Data.ParameterDirection.Input;
                    PaymentTypeId.Value = transactionDTO.PaymentTypeId;

                    SqlParameter VendorReferenceNo = cmd.Parameters.Add("@VendorReferenceNo", System.Data.SqlDbType.VarChar, 100);
                    VendorReferenceNo.Direction = System.Data.ParameterDirection.Input;
                    VendorReferenceNo.Value = transactionDTO.VendorReferenceNo;

                    SqlParameter PaymentReferenceNo = cmd.Parameters.Add("@PaymentReferenceNo", System.Data.SqlDbType.Int);
                    PaymentReferenceNo.Direction = System.Data.ParameterDirection.Input;
                    PaymentReferenceNo.Value = transactionDTO.PaymentReferenceNo;

                    SqlParameter PaymentDate = cmd.Parameters.Add("@PaymentDate", System.Data.SqlDbType.DateTime);
                    PaymentDate.Direction = System.Data.ParameterDirection.Input;
                    PaymentDate.Value = transactionDTO.PaymentDate;

                    SqlParameter BidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    BidId.Direction = System.Data.ParameterDirection.Input;
                    BidId.Value = transactionDTO.BidId;

                    SqlParameter Amount = cmd.Parameters.Add("@Amount", System.Data.SqlDbType.Decimal);
                    Amount.Direction = System.Data.ParameterDirection.Input;
                    Amount.Value = transactionDTO.Amount;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = transactionDTO.Status;

                    SqlParameter IsRefund = cmd.Parameters.Add("@IsRefund", System.Data.SqlDbType.Bit);
                    IsRefund.Direction = System.Data.ParameterDirection.Input;
                    IsRefund.Value = transactionDTO.IsRefund;

                    SqlParameter RefundAmount = cmd.Parameters.Add("@RefundAmount", System.Data.SqlDbType.Decimal);
                    RefundAmount.Direction = System.Data.ParameterDirection.Input;
                    RefundAmount.Value = transactionDTO.RefundAmount;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = transactionDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int AddMatchingBids(MatchingBidsDTO matchingBids)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddMatchingBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter BidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    BidId.Direction = System.Data.ParameterDirection.Input;
                    BidId.Value = matchingBids.BidId;

                    SqlParameter ServiceDayId = cmd.Parameters.Add("@ServiceDayId", System.Data.SqlDbType.Int);
                    ServiceDayId.Direction = System.Data.ParameterDirection.Input;
                    ServiceDayId.Value = matchingBids.ServiceDayId;

                    SqlParameter VendorDealId = cmd.Parameters.Add("@VendorDealId", System.Data.SqlDbType.Int);
                    VendorDealId.Direction = System.Data.ParameterDirection.Input;
                    VendorDealId.Value = matchingBids.VendorDealId;

                    SqlParameter SeatNo = cmd.Parameters.Add("@SeatNo", System.Data.SqlDbType.Int);
                    SeatNo.Direction = System.Data.ParameterDirection.Input;
                    SeatNo.Value = 0;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = matchingBids.Status;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = matchingBids.CreatedBy;


                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = matchingBids.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }




        internal static VendorDTO Login(string username, string password, int type)
        {
            VendorDTO v = new VendorDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[Login]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar, 100);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = username;

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 50);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = password;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.ContactName = (string)myData["Contact_Name"].ToString();
                        v.Phone1 = (string)myData["Phone1"].ToString();
                        v.Phone2 = (string)myData["Phone2"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Address1 = (string)myData["Address1"].ToString();
                        v.Address2 = (string)myData["Address2"].ToString();
                        v.Fax = (string)myData["Fax"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty) ? string.Empty : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty) ? string.Empty : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty) ? string.Empty : myData["Zip"].ToString();
                        v.Status = (int)myData["Status"];
                        v.Type = (int)myData["Type"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }


        internal static VendorUserDTO GetVendorUser(string username, int type)
        {
            VendorUserDTO v = new VendorUserDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorUser]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = username;

                    SqlParameter Type = cmd.Parameters.Add("@type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Phone = (string)myData["Phone"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Status = (int)myData["Status"];
                        v.Type = (int)myData["Type"];
                        v.UserType = (int)myData["UserType"];
                        v.VendorId = (int)myData["Vendor_Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static List<LocationDTO> GetLocationsByCityId(int cityId)
        {
            List<LocationDTO> locationsList = new List<LocationDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetLocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@cityId", System.Data.SqlDbType.Int);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = cityId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        LocationDTO locationDTO = new LocationDTO();
                        locationDTO.Id = (int)myData["ID"];
                        locationDTO.Address = (string)myData["Address"].ToString();
                        locationDTO.Name = (string)myData["Name"].ToString();
                        locationDTO.CityId = cityId;
                        locationsList.Add(locationDTO);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return locationsList;

        }

        internal static List<CustomerDTO> getRelatedCustomers(int Id)
        {

            List<CustomerDTO> locationsList = new List<CustomerDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRelatedCustomers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        CustomerDTO v = new CustomerDTO();
                        v.Id = (int)myData["id"];
                        v.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"].ToString();
                        v.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"].ToString();
                        v.DOB = (myData["DOB"] == DBNull.Value) ? "" : (string)myData["DOB"].ToString();
                        v.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"].ToString();
                        v.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.CityId = (myData["CityId"] == DBNull.Value) ? 0 : (int)myData["CityId"];



                        locationsList.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return locationsList;

        }


        internal static List<BusDTO> GetBuses()
        {

            List<BusDTO> Buses = new List<BusDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBuses]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        BusDTO v = new BusDTO();
                        v.Id = (int)myData["id"];
                        v.BusType = (myData["BusType"] == DBNull.Value) ? "" : (string)myData["BusType"].ToString();
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        v.IsAc = (myData["IsAc"] == DBNull.Value) ? 0 : (int)myData["IsAc"];
                        v.SKU = (myData["SKU"] == DBNull.Value) ? "" : (string)myData["SKU"].ToString();
                        v.BusTypeId = (myData["BusTypeId"] == DBNull.Value) ? 0 : (int)myData["BusTypeId"];
                        v.Status = (myData["Status"] == DBNull.Value) ? 0 : (int)myData["Status"];
                        Buses.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Buses;

        }



        internal static List<VendorRouteDTO> GetVendorRoutes(int VendorId)
        {

            List<VendorRouteDTO> vendorRouteDTO = new List<VendorRouteDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorRoutes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter vendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    vendorId.Direction = System.Data.ParameterDirection.Input;
                    vendorId.Value = VendorId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VendorRouteDTO vendorRoute = new VendorRouteDTO();

                        BusDTO bus = new BusDTO();
                        bus.Id = (int)myData["BusId"];
                        bus.BusType = (myData["BusType"] == DBNull.Value) ? "" : (string)myData["BusType"].ToString();
                        bus.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        bus.IsAc = (myData["IsAc"] == DBNull.Value) ? 0 : (int)myData["IsAc"];
                        bus.SKU = (myData["SKU"] == DBNull.Value) ? "" : (string)myData["SKU"].ToString();
                        bus.BusTypeId = (myData["BusTypeId"] == DBNull.Value) ? 0 : (int)myData["BusTypeId"];
                        vendorRoute.Bus = bus;
                        vendorRoute.Id = (int)myData["id"];
                        vendorRoute.BusId = (myData["BusId"] == DBNull.Value) ? 0 : (int)myData["BusId"];
                        vendorRoute.FromCity = (myData["FromCity"] == DBNull.Value) ? 0 : (int)myData["FromCity"];
                        vendorRoute.ToCity = (myData["ToCity"] == DBNull.Value) ? 0 : (int)myData["ToCity"];
                        vendorRoute.StartLocation = (myData["StartLocation"] == DBNull.Value) ? 0 : (int)myData["StartLocation"];
                        vendorRoute.EndLocation = (myData["EndLocation"] == DBNull.Value) ? 0 : (int)myData["EndLocation"];
                        vendorRoute.Distance = (myData["Distance"] == DBNull.Value) ? 0 : (int)myData["Distance"];
                        vendorRoute.RetailPrice = (myData["RetailPrice"] == DBNull.Value) ? 0 : (decimal)myData["RetailPrice"];
                        vendorRoute.SpecialPrice = (myData["SpecialPrice"] == DBNull.Value) ? 0 : (decimal)myData["SpecialPrice"];
                        vendorRoute.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"];
                        vendorRoute.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"];
                        vendorRoute.FromLocation = (myData["FromLocation"] == DBNull.Value) ? "" : (string)myData["FromLocation"];
                        vendorRoute.ToLocation = (myData["ToLocation"] == DBNull.Value) ? "" : (string)myData["ToLocation"];
                        vendorRoute.FromAddress = (myData["FromAddress"] == DBNull.Value) ? "" : (string)myData["FromAddress"];
                        vendorRoute.ToAddress = (myData["ToAddress"] == DBNull.Value) ? "" : (string)myData["ToAddress"];
                        vendorRouteDTO.Add(vendorRoute);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return vendorRouteDTO;

        }

        internal static List<ServiceDTO> GetService(int VendorRouteId, DateTime Date)
        {

            List<ServiceDTO> serviceDTO = new List<ServiceDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServices]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter vendorRouteId = cmd.Parameters.Add("@RouteId", System.Data.SqlDbType.Int);
                    vendorRouteId.Direction = System.Data.ParameterDirection.Input;
                    vendorRouteId.Value = VendorRouteId;



                    SqlParameter date = cmd.Parameters.Add("@Date", System.Data.SqlDbType.DateTime);
                    date.Direction = System.Data.ParameterDirection.Input;
                    date.Value = Date;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ServiceDTO service = new ServiceDTO();

                        service.Id = (int)myData["id"];
                        service.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        service.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        service.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        service.NoOfStops = (myData["NoOfStops"] == DBNull.Value) ? 0 : (int)myData["NoOfStops"];
                        service.RouteId = (myData["RouteId"] == DBNull.Value) ? 0 : (int)myData["RouteId"];
                        service.VendorId = (myData["VendorId"] == DBNull.Value) ? 0 : (int)myData["VendorId"];
                        service.BusTypeId = (myData["BusTypeId"] == DBNull.Value) ? 0 : (int)myData["BusTypeId"];
                        service.StartLocation = (myData["StartLocation"] == DBNull.Value) ? 0 : (int)myData["StartLocation"];
                        service.EndLocation = (myData["EndLocation"] == DBNull.Value) ? 0 : (int)myData["EndLocation"];
                        service.RetailPrice = (myData["RetailPrice"] == DBNull.Value) ? 0 : (decimal)myData["RetailPrice"];
                        service.SpecialPrice = (myData["SpecialPrice"] == DBNull.Value) ? 0 : (decimal)myData["SpecialPrice"];
                        service.FromDate = (myData["ServiceFromDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceFromDate"];
                        service.ToDate = (myData["ServiceToDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceToDate"];
                        serviceDTO.Add(service);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return serviceDTO;

        }


        internal static ServiceDTO GetServiceById(int ServiceId)
        {

            ServiceDTO service = new ServiceDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetService]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = ServiceId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        service.Id = (int)myData["id"];
                        service.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        service.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        service.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        service.NoOfStops = (myData["NoOfStops"] == DBNull.Value) ? 0 : (int)myData["NoOfStops"];
                        service.RouteId = (myData["RouteId"] == DBNull.Value) ? 0 : (int)myData["RouteId"];
                        service.VendorId = (myData["VendorId"] == DBNull.Value) ? 0 : (int)myData["VendorId"];
                        service.BusTypeId = (myData["BusTypeId"] == DBNull.Value) ? 0 : (int)myData["BusTypeId"];
                        service.StartLocation = (myData["StartLocation"] == DBNull.Value) ? 0 : (int)myData["StartLocation"];
                        service.EndLocation = (myData["EndLocation"] == DBNull.Value) ? 0 : (int)myData["EndLocation"];
                        service.RetailPrice = (myData["RetailPrice"] == DBNull.Value) ? 0 : (decimal)myData["RetailPrice"];
                        service.SpecialPrice = (myData["SpecialPrice"] == DBNull.Value) ? 0 : (decimal)myData["SpecialPrice"];
                        service.FromDate = (myData["ServiceFromDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceFromDate"];
                        service.ToDate = (myData["ServiceToDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceToDate"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return service;

        }

        internal static List<ServiceDTO> GetServiceByMerchants(int MerchantId)
        {

            List<ServiceDTO> serviceDTO = new List<ServiceDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServicesByVendors]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = MerchantId;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ServiceDTO service = new ServiceDTO();

                        service.Id = (int)myData["id"];
                        service.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        service.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        service.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        service.NoOfStops = (myData["NoOfStops"] == DBNull.Value) ? 0 : (int)myData["NoOfStops"];
                        service.RouteId = (myData["RouteId"] == DBNull.Value) ? 0 : (int)myData["RouteId"];
                        service.VendorId = (myData["VendorId"] == DBNull.Value) ? 0 : (int)myData["VendorId"];
                        service.BusTypeId = (myData["BusTypeId"] == DBNull.Value) ? 0 : (int)myData["BusTypeId"];
                        service.StartLocation = (myData["StartLocation"] == DBNull.Value) ? 0 : (int)myData["StartLocation"];
                        service.EndLocation = (myData["EndLocation"] == DBNull.Value) ? 0 : (int)myData["EndLocation"];
                        service.RetailPrice = (myData["RetailPrice"] == DBNull.Value) ? 0 : (decimal)myData["RetailPrice"];
                        service.SpecialPrice = (myData["SpecialPrice"] == DBNull.Value) ? 0 : (decimal)myData["SpecialPrice"];
                        service.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"];
                        service.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"];
                        service.FromDate = (myData["ServiceFromDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceFromDate"];
                        service.ToDate = (myData["ServiceToDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ServiceToDate"];
                        serviceDTO.Add(service);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return serviceDTO;

        }


        internal static List<VendorDealsDTO> GetVendorDeals(int ServiceId, DateTime Date)
        {

            List<VendorDealsDTO> vendorDealsDTO = new List<VendorDealsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorDeals]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = ServiceId;



                    SqlParameter date = cmd.Parameters.Add("@Date", System.Data.SqlDbType.DateTime);
                    date.Direction = System.Data.ParameterDirection.Input;
                    date.Value = Date;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VendorDealsDTO vendorDeals = new VendorDealsDTO();

                        vendorDeals.Id = (int)myData["id"];
                        vendorDeals.DiscountPercentage = (myData["DiscountPercentage"] == DBNull.Value) ? 0 : (int)myData["DiscountPercentage"];
                        vendorDeals.OfferedPrice = (myData["OfferedPrice"] == DBNull.Value) ? 0 : (decimal)myData["OfferedPrice"];
                        vendorDeals.AvailableSeats = (myData["AvailableSeats"] == DBNull.Value) ? 0 : (int)myData["AvailableSeats"];
                        vendorDeals.RemainingSeats = (myData["RemainingSeats"] == DBNull.Value) ? 0 : (int)myData["RemainingSeats"];
                        vendorDeals.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        vendorDeals.FromDate = (myData["StartDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["StartDate"];
                        vendorDeals.ToDate = (myData["EndDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["EndDate"];
                        vendorDeals.ServiceId = (myData["ServiceId"] == DBNull.Value) ? 0 : (int)myData["ServiceId"];
                        vendorDealsDTO.Add(vendorDeals);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return vendorDealsDTO;

        }


        internal static List<BidDTO> GetClosedBids(int MerchantId)
        {

            List<BidDTO> Bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClosedBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter merchantId = cmd.Parameters.Add("@MerchantId", System.Data.SqlDbType.Int);
                    merchantId.Direction = System.Data.ParameterDirection.Input;
                    merchantId.Value = MerchantId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidDTO v = new BidDTO();
                        ServiceDTO s = new ServiceDTO();
                        ServiceDaysDTO sd = new ServiceDaysDTO();
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        v.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["JourneyDate"];
                        v.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        v.Self = (myData["Self"] == DBNull.Value) ? true : (bool)myData["Self"];
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        v.ServiceDTO = s;
                        sd.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.ServiceDayDTO = sd;
                        Bids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Bids;

        }

        internal static List<BidDTO> GetBidsByStatus(int MerchantId, int Status, int Offset, int Max)
        {

            List<BidDTO> Bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBidsByStatus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter merchantId = cmd.Parameters.Add("@MerchantId", System.Data.SqlDbType.Int);
                    merchantId.Direction = System.Data.ParameterDirection.Input;
                    merchantId.Value = MerchantId;


                    SqlParameter status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = Status;

                    SqlParameter offset = cmd.Parameters.Add("@Offset", System.Data.SqlDbType.Int);
                    offset.Direction = System.Data.ParameterDirection.Input;
                    offset.Value = Offset;

                    SqlParameter max = cmd.Parameters.Add("@Max", System.Data.SqlDbType.Int);
                    max.Direction = System.Data.ParameterDirection.Input;
                    max.Value = Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidDTO v = new BidDTO();
                        ServiceDTO s = new ServiceDTO();
                        ServiceDaysDTO sd = new ServiceDaysDTO();
                        CustomerDTO customer = new CustomerDTO();
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        v.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["JourneyDate"];
                        v.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        v.Self = (myData["Self"] == DBNull.Value) ? true : (bool)myData["Self"];
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        v.fromCityId = (myData["fromcity"] == DBNull.Value) ? 0 : (int)myData["fromcity"];
                        v.toCityId = (myData["tocity"] == DBNull.Value) ? 0 : (int)myData["tocity"];
                        v.FromCityName = (myData["FromCityName"] == DBNull.Value) ? string.Empty : (string)myData["FromCityName"];
                        v.ToCityName = (myData["ToCityName"] == DBNull.Value) ? string.Empty : (string)myData["ToCityName"];
                        v.VendorName = (myData["VendorName"] == DBNull.Value) ? string.Empty : (string)myData["VendorName"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        v.ServiceDTO = s;
                        sd.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.ServiceDayDTO = sd;
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        customer.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        customer.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];
                        customer.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"];
                        customer.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"];
                        v.customerInfo = customer;
                        Bids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Bids;

        }


        internal static CustomerDTO authenticateUser(string email, string password)
        {

            CustomerDTO customerDTO = null;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AuthenticateUser]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter merchantId = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 50);
                    merchantId.Direction = System.Data.ParameterDirection.Input;
                    merchantId.Value = email;

                    SqlParameter Password = cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar, 50);
                    Password.Direction = System.Data.ParameterDirection.Input;
                    Password.Value = password;

                    SqlDataReader myData = cmd.ExecuteReader();
                    if (myData.HasRows)
                        while (myData.Read())
                        {
                            customerDTO = new CustomerDTO();
                            customerDTO.Id = Convert.ToInt32(myData["ID"]);
                            customerDTO.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"].ToString();
                            customerDTO.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"].ToString();
                            customerDTO.DOB = (myData["DOB"] == DBNull.Value) ? "" : (string)myData["DOB"].ToString();
                            customerDTO.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"].ToString();
                            customerDTO.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"].ToString();
                            customerDTO.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                            customerDTO.CityId = (myData["CityId"] == DBNull.Value) ? 0 : (int)myData["CityId"];
                            customerDTO.Password = (myData["Password"] == DBNull.Value) ? string.Empty : (string)myData["Password"];
                        }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return customerDTO;

        }

        internal static int AcceptBid(int BidId, int AcceptedBy, int CreatedBy)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAcceptedByForBid]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;

                    SqlParameter acceptedBy = cmd.Parameters.Add("@AcceptedBy", System.Data.SqlDbType.Int);
                    acceptedBy.Direction = System.Data.ParameterDirection.Input;
                    acceptedBy.Value = AcceptedBy;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = CreatedBy;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return BidId;
        }



        internal static List<SeatDTO> AllocateSeatNo(int BidId, string SeatNo, int CreatedBy)
        {

            List<SeatDTO> seatDTO = new List<SeatDTO>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateSeatNoForBid]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;

                    SqlParameter seatNo = cmd.Parameters.Add("@SeatNo", System.Data.SqlDbType.VarChar, 100);
                    seatNo.Direction = System.Data.ParameterDirection.Input;
                    seatNo.Value = SeatNo;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SeatDTO s = new SeatDTO();
                        s.Id = (int)myData["id"];
                        s.SeatNo = (myData["SeatNumber"] == DBNull.Value) ? string.Empty : (string)myData["SeatNumber"];
                        s.BidId = (myData["BidId"] == DBNull.Value) ? 0 : (int)myData["BidId"];
                        s.Status = (myData["Status"] == DBNull.Value) ? 0 : (int)myData["Status"];
                        s.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        s.CustomerName = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        seatDTO.Add(s);

                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return seatDTO;
        }


        internal static int AddBusType(BusTypesDTO busTypesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBusType]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter busType = cmd.Parameters.Add("@BusType", System.Data.SqlDbType.VarChar, 50);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = busTypesDTO.Name;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = busTypesDTO.CreatedBy;

                    SqlParameter CategoryId = cmd.Parameters.Add("@CategoryId", System.Data.SqlDbType.Int);
                    CategoryId.Direction = System.Data.ParameterDirection.Input;
                    CategoryId.Value = busTypesDTO.CategoryId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = busTypesDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int AddCity(CityDTO cityDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCity]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter city_name = cmd.Parameters.Add("@city_name", System.Data.SqlDbType.VarChar, 50);
                    city_name.Direction = System.Data.ParameterDirection.Input;
                    city_name.Value = cityDTO.Name;

                    SqlParameter city_state = cmd.Parameters.Add("@city_state", System.Data.SqlDbType.VarChar, 15);
                    city_state.Direction = System.Data.ParameterDirection.Input;
                    city_state.Value = cityDTO.State;

                    SqlParameter zip = cmd.Parameters.Add("@zip", System.Data.SqlDbType.Int);
                    zip.Direction = System.Data.ParameterDirection.Input;
                    zip.Value = cityDTO.Zip;


                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = cityDTO.CreatedBy;


                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = cityDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteBusType(int BusTypeId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteBusType]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    id.Direction = System.Data.ParameterDirection.Input;
                    id.Value = BusTypeId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int AddRoute(RouteDTO routeDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddRoute]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter FromCity = cmd.Parameters.Add("@FromCity", System.Data.SqlDbType.Int);
                    FromCity.Direction = System.Data.ParameterDirection.Input;
                    FromCity.Value = routeDTO.FromCity;

                    SqlParameter ToCity = cmd.Parameters.Add("@ToCity", System.Data.SqlDbType.Int);
                    ToCity.Direction = System.Data.ParameterDirection.Input;
                    ToCity.Value = routeDTO.ToCity;

                    SqlParameter Distance = cmd.Parameters.Add("@Distance", System.Data.SqlDbType.VarChar, 50);
                    Distance.Direction = System.Data.ParameterDirection.Input;
                    Distance.Value = routeDTO.Distance;


                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = routeDTO.Status;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.Int);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = routeDTO.Type;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = routeDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = routeDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<RouteDTO> GetRoutes(int routeId)
        {
            List<RouteDTO> RoutesList = new List<RouteDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRoutes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter RouteId = cmd.Parameters.Add("@RouteId", System.Data.SqlDbType.Int);
                    RouteId.Direction = System.Data.ParameterDirection.Input;
                    RouteId.Value = routeId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        RouteDTO Routes = new RouteDTO();
                        Routes.Id = (int)myData["id"];
                        Routes.FromCity = (myData["FromCity"] == DBNull.Value) ? 0 : (int)myData["FromCity"];
                        Routes.ToCity = (myData["ToCity"] == DBNull.Value) ? 0 : (int)myData["ToCity"];
                        Routes.Distance = (myData["Distance"] == DBNull.Value) ? 0 : (int)myData["Distance"];
                        Routes.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"].ToString();
                        Routes.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"].ToString();
                        Routes.Status = (myData["Status"] == DBNull.Value) ? 0 : (int)myData["Status"];
                        Routes.Type = (myData["Type"] == DBNull.Value) ? 0 : (int)myData["Type"];
                        RoutesList.Add(Routes);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return RoutesList;
        }

        internal static int DeleteRoute(int RouteId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteRoute]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter routeId = cmd.Parameters.Add("@RouteId", System.Data.SqlDbType.Int);
                    routeId.Direction = System.Data.ParameterDirection.Input;
                    routeId.Value = RouteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int DeleteService(int ServiceId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteService]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = ServiceId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int RegisterDevice(DeviceDTO deviceDTO)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RegisterDevice]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CustomerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    CustomerId.Direction = System.Data.ParameterDirection.Input;
                    CustomerId.Value = deviceDTO.CustomerId;

                    SqlParameter UDID = cmd.Parameters.Add("@UDID", System.Data.SqlDbType.VarChar, 100);
                    UDID.Direction = System.Data.ParameterDirection.Input;
                    UDID.Value = deviceDTO.UDID;

                    SqlParameter PushId = cmd.Parameters.Add("@PushId", System.Data.SqlDbType.VarChar, 1000);
                    PushId.Direction = System.Data.ParameterDirection.Input;
                    PushId.Value = deviceDTO.PushId;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", System.Data.SqlDbType.VarChar, 50);
                    DeviceType.Direction = System.Data.ParameterDirection.Input;
                    DeviceType.Value = deviceDTO.DeviceType;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();


                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }

        internal static bool PushNotification(string pushMessage, string Name)
        {
            bool isPushMessageSend = false;
            try
            {
                string postString = "";
                string urlpath = "https://api.parse.com/1/push";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlpath);
                postString = "{ \"channels\": [ \"" + Name + "\"  ], " +
                                 "\"data\" : {\"alert\":\"" + pushMessage + "\"}" +
                                 "}";
                //postString = "{ \"channels\": [ \"Biolauniversity\"  ], " +
                //                 "\"data\" : {\"alert\":\"" + pushMessage + "\"}" +
                //                 "}";

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = postString.Length;
                httpWebRequest.Headers.Add("X-Parse-Application-Id", "LSiKPwYiKv8xbTTWzUjcNIja1OY0mcoQ02R2Iiw1");
                httpWebRequest.Headers.Add("X-Parse-REST-API-KEY", "8tPrMTYWmfd6OgijXQkpS0gKh4D3ACiszMGusqiS");
                httpWebRequest.Method = "POST";
                StreamWriter requestWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    JObject jObjRes = JObject.Parse(responseText);
                    if (Convert.ToString(jObjRes).IndexOf("true") != -1)
                    {
                        isPushMessageSend = true;
                    }
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return isPushMessageSend;
        }


        internal static RouteLocationsDTO GetRouteLocations(int routeId)
        {
            RouteLocationsDTO RouteLocations = new RouteLocationsDTO();
            List<LocationDTO> FromCityLocations = new List<LocationDTO>();
            List<LocationDTO> ToCityLocations = new List<LocationDTO>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRouteLocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter RouteId = cmd.Parameters.Add("@RouteId", System.Data.SqlDbType.Int);
                    RouteId.Direction = System.Data.ParameterDirection.Input;
                    RouteId.Value = routeId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        LocationDTO FromCityLocation = new LocationDTO();
                        LocationDTO ToCityLocation = new LocationDTO();
                        FromCityLocation.Id = (int)myData["FromId"];
                        FromCityLocation.CityName = (string)myData["FromName"];
                        FromCityLocation.Address = (string)myData["FromAddress"];
                        ToCityLocation.Id = (int)myData["ToId"];
                        ToCityLocation.CityName = (string)myData["ToName"];
                        ToCityLocation.Address = (string)myData["ToAddress"];
                        FromCityLocations.Add(FromCityLocation);
                        ToCityLocations.Add(ToCityLocation);

                    }

                    RouteLocations.FromCityLocations = RemoveDuplicates(FromCityLocations);
                    RouteLocations.ToCityLocations = RemoveDuplicates(ToCityLocations);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return RouteLocations;
        }

        private static List<LocationDTO> RemoveDuplicates(List<LocationDTO> FromCityLocations)
        {
            List<LocationDTO> newDuplicateList = new List<LocationDTO>();
            for (int i = 0; i < FromCityLocations.Count; i++)
            {
                int Id = FromCityLocations[i].Id;
                if (!newDuplicateList.Any(s => s.Id == Id))
                {
                    //Get the list 
                    newDuplicateList.Add(FromCityLocations[i]);
                }
            }

            return newDuplicateList;
        }


        internal static int AddException(ExceptionDTO exceptionDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddException]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Controller = cmd.Parameters.Add("@Controller", System.Data.SqlDbType.VarChar, 50);
                    Controller.Direction = System.Data.ParameterDirection.Input;
                    Controller.Value = exceptionDTO.Controller;

                    SqlParameter ActionMethod = cmd.Parameters.Add("@ActionMethod", System.Data.SqlDbType.VarChar, 50);
                    ActionMethod.Direction = System.Data.ParameterDirection.Input;
                    ActionMethod.Value = exceptionDTO.ActionMethod;

                    SqlParameter ExceptionText = cmd.Parameters.Add("@ExceptionText", System.Data.SqlDbType.VarChar, 1000);
                    ExceptionText.Direction = System.Data.ParameterDirection.Input;
                    ExceptionText.Value = exceptionDTO.ExceptionText;

                    SqlParameter StackTrace = cmd.Parameters.Add("@StackTrace", System.Data.SqlDbType.VarChar, 4000);
                    StackTrace.Direction = System.Data.ParameterDirection.Input;
                    StackTrace.Value = exceptionDTO.StackTrace;

                    SqlParameter ExceptionRaisedTo = cmd.Parameters.Add("@ExceptionRaisedTo", System.Data.SqlDbType.Int);
                    ExceptionRaisedTo.Direction = System.Data.ParameterDirection.Input;
                    ExceptionRaisedTo.Value = exceptionDTO.ExceptionRaisedTo;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = exceptionDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static List<ExceptionDTO> GetExceptions(int userid)
        {
            List<ExceptionDTO> exception = new List<ExceptionDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetExceptions]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter UserId = cmd.Parameters.Add("@UserId", System.Data.SqlDbType.Int);
                    UserId.Direction = System.Data.ParameterDirection.Input;
                    UserId.Value = userid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ExceptionDTO v = new ExceptionDTO();
                        v.Id = (long)myData["id"];
                        v.Controller = (myData["Controller"] == DBNull.Value) ? "" : (string)myData["Controller"].ToString();
                        v.ActionMethod = (myData["ActionMethod"] == DBNull.Value) ? "" : (string)myData["ActionMethod"].ToString();
                        v.ExceptionText = (myData["ExceptionText"] == DBNull.Value) ? "" : (string)myData["ExceptionText"].ToString();
                        v.StackTrace = (myData["StackTrace"] == DBNull.Value) ? "" : (string)myData["StackTrace"].ToString();
                        v.ExceptionDate = (myData["ExceptionDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ExceptionDate"];
                        v.ExceptionRaisedTo = (myData["ExceptionRaisedTo"] == DBNull.Value) ? 0 : (int)myData["ExceptionRaisedTo"];
                        exception.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return exception;
        }


        internal static int GetUserType(int userid)
        {
            int type = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserType]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter UserId = cmd.Parameters.Add("@UserId", System.Data.SqlDbType.VarChar, 100);
                    UserId.Direction = System.Data.ParameterDirection.Input;
                    UserId.Value = userid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        type = (int)myData["usertype"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return type;
        }


        internal static int DeleteBus(int BusId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteBus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter busId = cmd.Parameters.Add("@BusId", System.Data.SqlDbType.Int);
                    busId.Direction = System.Data.ParameterDirection.Input;
                    busId.Value = BusId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int DeleteCity(int CityId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCity]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter cityId = cmd.Parameters.Add("@CityId", System.Data.SqlDbType.Int);
                    cityId.Direction = System.Data.ParameterDirection.Input;
                    cityId.Value = CityId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }



        internal static int DeleteLocation(int LocationId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteLocation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter locationId = cmd.Parameters.Add("@LocationId", System.Data.SqlDbType.Int);
                    locationId.Direction = System.Data.ParameterDirection.Input;
                    locationId.Value = LocationId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }

        internal static int DeleteMerchant(int VendorId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteVendor]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    vendorId.Direction = System.Data.ParameterDirection.Input;
                    vendorId.Value = VendorId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }

        internal static int DeleteServiceStops(int ServiceId, int LocationId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteServicelocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = ServiceId;

                    SqlParameter locationId = cmd.Parameters.Add("@LocationId", System.Data.SqlDbType.Int);
                    locationId.Direction = System.Data.ParameterDirection.Input;
                    locationId.Value = LocationId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }



        internal static int DeleteServiceAmenities(int ServiceId, int AmenityId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteServiceAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = ServiceId;

                    SqlParameter amenityId = cmd.Parameters.Add("AmenityId", System.Data.SqlDbType.Int);
                    amenityId.Direction = System.Data.ParameterDirection.Input;
                    amenityId.Value = AmenityId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int GetServiceDayForService(int serviceId, DateTime date)
        {
            int serviceDayId = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServiceDayForService]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlParameter Date = cmd.Parameters.Add("@Date", System.Data.SqlDbType.DateTime);
                    Date.Direction = System.Data.ParameterDirection.Input;
                    Date.Value = date;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        serviceDayId = (int)myData["ServiceDayId"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return serviceDayId;
        }


        internal static List<BidDTO> GetServicesForRoute(int BidId, int MerchantId)
        {

            List<BidDTO> bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetServicesForRoute]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;

                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = MerchantId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ServiceDTO service = new ServiceDTO();
                        BidDTO bid = new BidDTO();
                        service.Id = (int)myData["serviceid"];
                        service.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        service.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        service.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        service.VendorId = (myData["vendorId"] == DBNull.Value) ? 0 : (int)myData["vendorId"];
                        service.BusType = (myData["BusType"] == DBNull.Value) ? "" : (string)myData["BusType"];
                        bid.ServiceDTO = service;
                        bid.Id = (int)myData["Id"];
                        bid.fromCityId = (myData["fromcity"] == DBNull.Value) ? 0 : (int)myData["fromcity"];
                        bid.toCityId = (myData["tocity"] == DBNull.Value) ? 0 : (int)myData["tocity"];
                        bid.FromCityName = (myData["fromcityname"] == DBNull.Value) ? "" : (string)myData["fromcityname"];
                        bid.ToCityName = (myData["tocityname"] == DBNull.Value) ? "" : (string)myData["tocityname"];
                        bid.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("2015-09-09") : (DateTime)myData["JourneyDate"];
                        bid.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        bid.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        bid.ServiceDayId = (myData["servicedayid"] == DBNull.Value) ? 0 : (int)myData["servicedayid"];
                        bid.RouteId = (myData["RouteId"] == DBNull.Value) ? 0 : (int)myData["RouteId"];
                        bid.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        bids.Add(bid);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return bids;

        }

        internal static int UpdateBidStatus(int BidId, int Status, int changedby)
        {

            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateBidStatus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;

                    SqlParameter status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = Status;

                    SqlParameter ChangedBy = cmd.Parameters.Add("@ChangedBy", System.Data.SqlDbType.Int);
                    ChangedBy.Direction = System.Data.ParameterDirection.Input;
                    ChangedBy.Value = changedby;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];

                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int CheckServiceDayExists(int BidId)
        {
            List<int> serviceDays = new List<int>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckServiceDayExists]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        int v = 0;
                        v = (int)myData["Id"];
                        serviceDays.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return serviceDays.Count;
        }


        internal static void CheckForCompletedBids()
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckForCompletedBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }


        internal static List<VendorUserDTO> GetVendorUsersByVendor(int vendorId)
        {
            List<VendorUserDTO> vendorUsers = new List<VendorUserDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorUsers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Vendorid = cmd.Parameters.Add("@Vendorid", System.Data.SqlDbType.Int);
                    Vendorid.Direction = System.Data.ParameterDirection.Input;
                    Vendorid.Value = vendorId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VendorUserDTO v = new VendorUserDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Phone = (string)myData["Phone"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Status = (int)myData["Status"];
                        v.Type = (int)myData["Type"];
                        v.UserType = (int)myData["UserType"];
                        vendorUsers.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return vendorUsers;
        }


        internal static int DeleteAgent(int AgentId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteVendorUser]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vendorUserId = cmd.Parameters.Add("@VendorUserId", System.Data.SqlDbType.Int);
                    vendorUserId.Direction = System.Data.ParameterDirection.Input;
                    vendorUserId.Value = AgentId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static VendorDTO GetMerchant(string username)
        {
            VendorDTO Vendor = new VendorDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetMerchant]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = username;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Vendor.Id = (int)myData["id"];
                        Vendor.Name = (string)myData["Name"].ToString();
                        Vendor.ContactName = (string)myData["Contact_Name"].ToString();
                        Vendor.Address1 = (string)myData["Address1"].ToString();
                        Vendor.Email = (string)myData["Email"].ToString();
                        Vendor.Address2 = (string)myData["Address2"].ToString();
                        Vendor.City = (string)myData["City"].ToString();
                        Vendor.State = (string)myData["State"].ToString();
                        Vendor.Zip = (string)myData["Zip"].ToString();
                        Vendor.Phone1 = (string)myData["Phone1"].ToString();
                        Vendor.Phone2 = (string)myData["Phone2"].ToString();
                        Vendor.Fax = (string)myData["Fax"].ToString();
                        Vendor.UserType = 2;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Vendor;
        }


        internal static VendorUserDTO GetVendorUserByID(int VendoruserId)
        {
            VendorUserDTO v = new VendorUserDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorUserById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vendoruserId = cmd.Parameters.Add("@VendorUserId", System.Data.SqlDbType.Int);
                    vendoruserId.Direction = System.Data.ParameterDirection.Input;
                    vendoruserId.Value = VendoruserId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Phone = (string)myData["Phone"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Status = (int)myData["Status"];
                        v.Type = (int)myData["Type"];
                        v.UserType = (int)myData["UserType"];
                        v.VendorId = (int)myData["Vendor_Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }



        internal static List<VendorDealsDTO> GetAllDealsByVendorId(int VendorId)
        {

            List<VendorDealsDTO> vendorDealsDTO = new List<VendorDealsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorDealsByVendorId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter vendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    vendorId.Direction = System.Data.ParameterDirection.Input;
                    vendorId.Value = VendorId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VendorDealsDTO vendorDeals = new VendorDealsDTO();

                        vendorDeals.Id = (int)myData["id"];
                        vendorDeals.DiscountPercentage = (myData["DiscountPercentage"] == DBNull.Value) ? 0 : (int)myData["DiscountPercentage"];
                        vendorDeals.OfferedPrice = (myData["OfferedPrice"] == DBNull.Value) ? 0 : (decimal)myData["OfferedPrice"];
                        vendorDeals.AvailableSeats = (myData["AvailableSeats"] == DBNull.Value) ? 0 : (int)myData["AvailableSeats"];
                        vendorDeals.RemainingSeats = (myData["RemainingSeats"] == DBNull.Value) ? 0 : (int)myData["RemainingSeats"];
                        vendorDeals.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        vendorDeals.FromDate = (myData["StartDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["StartDate"];
                        vendorDeals.ToDate = (myData["EndDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["EndDate"];
                        vendorDeals.ServiceId = (myData["ServiceId"] == DBNull.Value) ? 0 : (int)myData["ServiceId"];

                        vendorDealsDTO.Add(vendorDeals);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return vendorDealsDTO;

        }



        internal static VendorDealsDTO GetDealsById(int VendorDealId)
        {

            VendorDealsDTO vendorDeals = new VendorDealsDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorDealsById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter vendorDealId = cmd.Parameters.Add("@VendorDealId", System.Data.SqlDbType.Int);
                    vendorDealId.Direction = System.Data.ParameterDirection.Input;
                    vendorDealId.Value = VendorDealId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        vendorDeals.Id = (int)myData["id"];
                        vendorDeals.DiscountPercentage = (myData["DiscountPercentage"] == DBNull.Value) ? 0 : (int)myData["DiscountPercentage"];
                        vendorDeals.OfferedPrice = (myData["OfferedPrice"] == DBNull.Value) ? 0 : (decimal)myData["OfferedPrice"];
                        vendorDeals.AvailableSeats = (myData["AvailableSeats"] == DBNull.Value) ? 0 : (int)myData["AvailableSeats"];
                        vendorDeals.RemainingSeats = (myData["RemainingSeats"] == DBNull.Value) ? 0 : (int)myData["RemainingSeats"];
                        vendorDeals.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        vendorDeals.FromDate = (myData["StartDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["StartDate"];
                        vendorDeals.ToDate = (myData["EndDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["EndDate"];
                        vendorDeals.ServiceId = (myData["ServiceId"] == DBNull.Value) ? 0 : (int)myData["ServiceId"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return vendorDeals;

        }


        internal static int DeleteVendorDeal(int VendorDealId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteVendorDeal]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vendordealId = cmd.Parameters.Add("@VendorDealId", System.Data.SqlDbType.Int);
                    vendordealId.Direction = System.Data.ParameterDirection.Input;
                    vendordealId.Value = VendorDealId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return i;
        }


        internal static int AddVendorPaymentDetails(VendorPaymentDTO vendorPaymentDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVendorPaymentDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter VendorId = cmd.Parameters.Add("@VendorId", System.Data.SqlDbType.Int);
                    VendorId.Direction = System.Data.ParameterDirection.Input;
                    VendorId.Value = vendorPaymentDTO.VendorId;

                    SqlParameter BankName = cmd.Parameters.Add("@BankName", System.Data.SqlDbType.VarChar, 100);
                    BankName.Direction = System.Data.ParameterDirection.Input;
                    BankName.Value = vendorPaymentDTO.BankName;

                    SqlParameter BranchName = cmd.Parameters.Add("@BranchName", System.Data.SqlDbType.VarChar, 100);
                    BranchName.Direction = System.Data.ParameterDirection.Input;
                    BranchName.Value = vendorPaymentDTO.BranchName;

                    SqlParameter AccountNumber = cmd.Parameters.Add("@AccountNumber", System.Data.SqlDbType.VarChar, 500);
                    AccountNumber.Direction = System.Data.ParameterDirection.Input;
                    AccountNumber.Value = vendorPaymentDTO.AccountNumber;

                    SqlParameter ReferenceNumber = cmd.Parameters.Add("@IFSC", System.Data.SqlDbType.VarChar, 500);
                    ReferenceNumber.Direction = System.Data.ParameterDirection.Input;
                    ReferenceNumber.Value = vendorPaymentDTO.IFSC;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = vendorPaymentDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.Output;
                    Id.Value = vendorPaymentDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static BidStatisticsDTO GetBidStatistics(int UserId)
        {

            BidStatisticsDTO BidStatistics = new BidStatisticsDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBidStatistics]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter userId = cmd.Parameters.Add("@UserId", System.Data.SqlDbType.Int);
                    userId.Direction = System.Data.ParameterDirection.Input;
                    userId.Value = UserId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidStatistics.Open = (myData["OpenBids"] == DBNull.Value) ? 0 : (int)myData["OpenBids"];
                        BidStatistics.Processing = (myData["ProcessingBids"] == DBNull.Value) ? 0 : (int)myData["ProcessingBids"];
                        BidStatistics.Processed = (myData["ProcessedBids"] == DBNull.Value) ? 0 : (int)myData["ProcessedBids"];
                        BidStatistics.Completed = (myData["CompletedBids"] == DBNull.Value) ? 0 : (int)myData["CompletedBids"];
                        BidStatistics.Archieved = (myData["ArchivedBids"] == DBNull.Value) ? 0 : (int)myData["ArchivedBids"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return BidStatistics;

        }



        internal static List<VendorDTO> GetVendorByBidId(int BidId)
        {
            List<VendorDTO> Vendor = new List<VendorDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVendorByBidId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter bidId = cmd.Parameters.Add("@BidId", System.Data.SqlDbType.Int);
                    bidId.Direction = System.Data.ParameterDirection.Input;
                    bidId.Value = BidId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        VendorDTO v = new VendorDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.ContactName = (string)myData["Contact_Name"].ToString();
                        v.Address1 = (string)myData["Address1"].ToString();
                        v.Email = (string)myData["Email"].ToString();
                        v.Address2 = (string)myData["Address2"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.Phone1 = (string)myData["Phone1"].ToString();
                        v.Phone2 = (string)myData["Phone2"].ToString();
                        v.Fax = (string)myData["Fax"].ToString();
                        v.Base64String = (myData["Logo"] == DBNull.Value) ? string.Empty : (string)myData["Logo"].ToString();
                        v.BankName = (string)myData["BankName"].ToString();
                        v.BranchName = (string)myData["BranchName"].ToString();
                        v.AccountNumber = (string)myData["AccountNumber"].ToString();
                        v.IFSC = (string)myData["IFSC"].ToString();
                        Vendor.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Vendor;
        }


        internal static List<CancelReasonsDTO> GetCancelReasons()
        {
            List<CancelReasonsDTO> Vendor = new List<CancelReasonsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCancelReasons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CancelReasonsDTO v = new CancelReasonsDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Status = (int)myData["Status"];
                        Vendor.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Vendor;
        }



        internal static int CancelBid(CancelBidsDTO cancelBidsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CancelBid]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Bidid = cmd.Parameters.Add("@Bidid", System.Data.SqlDbType.Int);
                    Bidid.Direction = System.Data.ParameterDirection.Input;
                    Bidid.Value = cancelBidsDTO.BidId;

                    SqlParameter ReasonId = cmd.Parameters.Add("@ReasonId", System.Data.SqlDbType.Int);
                    ReasonId.Direction = System.Data.ParameterDirection.Input;
                    ReasonId.Value = cancelBidsDTO.ReasonId;

                    SqlParameter Description = cmd.Parameters.Add("@Description", System.Data.SqlDbType.VarChar, 1000);
                    Description.Direction = System.Data.ParameterDirection.Input;
                    Description.Value = cancelBidsDTO.Description;

                    SqlParameter CancelledBy = cmd.Parameters.Add("@CancelledBy", System.Data.SqlDbType.Int);
                    CancelledBy.Direction = System.Data.ParameterDirection.Input;
                    CancelledBy.Value = cancelBidsDTO.CancelledBy;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", System.Data.SqlDbType.Int);
                    UserType.Direction = System.Data.ParameterDirection.Input;
                    UserType.Value = cancelBidsDTO.UserType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static List<CancelBidsDTO> GetCancelBids(int UserId)
        {

            List<CancelBidsDTO> cancelBids = new List<CancelBidsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCancelBids]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter userId = cmd.Parameters.Add("@UserId", System.Data.SqlDbType.Int);
                    userId.Direction = System.Data.ParameterDirection.Input;
                    userId.Value = UserId;

                    //SqlParameter userType = cmd.Parameters.Add("@usertype", System.Data.SqlDbType.Int);
                    //userType.Direction = System.Data.ParameterDirection.Input;
                    //userType.Value = UserType;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CancelBidsDTO c = new CancelBidsDTO();
                        c.BidId = (myData["BidId"] == DBNull.Value) ? 0 : (int)myData["BidId"];
                        c.ReasonId = (myData["ReasonId"] == DBNull.Value) ? 0 : (int)myData["ReasonId"];
                        c.Description = (myData["Description"] == DBNull.Value) ? string.Empty : (string)myData["Description"];
                        c.CancelledBy = (myData["CancelledBy"] == DBNull.Value) ? 0 : (int)myData["CancelledBy"];
                        c.UserType = (myData["UserType"] == DBNull.Value) ? 0 : (int)myData["UserType"];
                        c.CancelledName = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        c.CancelledOn = (myData["CreatedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["CreatedDate"];
                        c.ReasonName = (myData["ReasonName"] == DBNull.Value) ? string.Empty : (string)myData["ReasonName"];
                        c.VendorName = (myData["VendorName"] == DBNull.Value) ? string.Empty : (string)myData["VendorName"];
                        c.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["CancelledBy"];
                        cancelBids.Add(c);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return cancelBids;

        }

        internal static List<BusCategoriesDTO> GetBusCategories()
        {
            List<BusCategoriesDTO> busCategories = new List<BusCategoriesDTO>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBusCategories]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        BusCategoriesDTO bus = new BusCategoriesDTO();
                        bus.CategoryId = (myData["CategoryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CategoryId"]);
                        bus.CategoryName = (myData["BusCategoryType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["BusCategoryType"]);
                        busCategories.Add(bus);
                    }
                }
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return busCategories;
        }

        internal static string GetCustomerExists(string emailaddress)
        {

            string code = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCustomerExists]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", System.Data.SqlDbType.VarChar, 100);
                    Emailaddress.Direction = System.Data.ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        code = (string)myData["Code"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return code;
        }

        internal static string UpdateCustomerPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCustomerPassword]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Token = cmd.Parameters.Add("@Token", System.Data.SqlDbType.VarChar, 500);
                    Token.Direction = System.Data.ParameterDirection.Input;
                    Token.Value = token;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", System.Data.SqlDbType.VarChar, 100);
                    Emailaddress.Direction = System.Data.ParameterDirection.Input;
                    Emailaddress.Value = userName;

                    SqlParameter Password = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 100);
                    Password.Direction = System.Data.ParameterDirection.Input;
                    Password.Value = confirmPassword;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        userName = (string)myData["Email"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return userName;
        }

        internal static List<LocationsDTO> CheckForStartLocations(string fromRoute, string locations)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();
                var location = new List<LocationsDTO>();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckStartLocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Locations = cmd.Parameters.Add("@Locations", System.Data.SqlDbType.VarChar, 1000);
                    Locations.Direction = System.Data.ParameterDirection.Input;
                    Locations.Value = locations;

                    SqlParameter FromRoute = cmd.Parameters.Add("@FromRoute", System.Data.SqlDbType.VarChar, 100);
                    FromRoute.Direction = System.Data.ParameterDirection.Input;
                    FromRoute.Value = fromRoute;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        LocationsDTO locate = new LocationsDTO();
                        locate.LocationName = (myData["splitdata"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["splitdata"]);
                        locate.Status = (myData["result"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["result"]);
                        location.Add(locate);
                    }
                    myData.Close();
                    myConn.Close();
                }
                return location;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }


        internal static void CheckForCompatibleLocations(string fromRoute, string endRoute, ref int fromRouteId, ref int toRouteId)
        {
            try
            {

                SqlConnection myConn = ConnectTODB();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ValidateLocations]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter StartLocations = cmd.Parameters.Add("@StartLocation", System.Data.SqlDbType.VarChar, 100);
                    StartLocations.Direction = System.Data.ParameterDirection.Input;
                    StartLocations.Value = fromRoute;

                    SqlParameter EndLocations = cmd.Parameters.Add("@EndLocation", System.Data.SqlDbType.VarChar, 100);
                    EndLocations.Direction = System.Data.ParameterDirection.Input;
                    EndLocations.Value = endRoute;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        fromRouteId = (myData["STARTCITY"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["STARTCITY"]);
                        toRouteId = (myData["ENDCITY"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ENDCITY"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int GetBusTypeId(string busType)
        {
            int busTypeId = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ValidateBusTypes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter BusTypes = cmd.Parameters.Add("@BusType", System.Data.SqlDbType.VarChar, 100);
                    BusTypes.Direction = System.Data.ParameterDirection.Input;
                    BusTypes.Value = busType.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        busTypeId = (myData["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ID"]);
                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return busTypeId;
        }

        internal static void GetFromCityToCity(string fromRoute, string toRoute, string startLocation, string endLocation, ref int fromCityId, ref int toCityId, ref int startLocationId, ref int endLocationId, ref int routeId)
        {
            try
            {

                SqlConnection myConn = ConnectTODB();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ValidateFromTo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter fromCityParameter = cmd.Parameters.Add("@FromCity", System.Data.SqlDbType.VarChar, 50);
                    fromCityParameter.Direction = System.Data.ParameterDirection.Input;
                    fromCityParameter.Value = fromRoute.Trim();

                    SqlParameter toCityParameter = cmd.Parameters.Add("@ToCity", System.Data.SqlDbType.VarChar, 50);
                    toCityParameter.Direction = System.Data.ParameterDirection.Input;
                    toCityParameter.Value = toRoute.Trim();

                    SqlParameter startLocations = cmd.Parameters.Add("@StartLocation", System.Data.SqlDbType.VarChar, 50);
                    startLocations.Direction = System.Data.ParameterDirection.Input;
                    startLocations.Value = startLocation.Trim();

                    SqlParameter endLocations = cmd.Parameters.Add("@EndLocation", System.Data.SqlDbType.VarChar, 50);
                    endLocations.Direction = System.Data.ParameterDirection.Input;
                    endLocations.Value = endLocation.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        fromCityId = (myData["FROMCITY"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["FROMCITY"]);
                        startLocationId = (myData["StartLocation"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StartLocation"]);
                        toCityId = (myData["ToCity"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ToCity"]);
                        endLocationId = (myData["EndLocation"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["EndLocation"]);
                        routeId = (myData["RouteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["RouteId"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static int GetDayTypeId(string dayType, ref int DayTypeId)
        {
            int busTypeId = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDayTypeId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter DayTypes = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 50);
                    DayTypes.Direction = System.Data.ParameterDirection.Input;
                    DayTypes.Value = dayType.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        DayTypeId = (myData["ID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["ID"]);
                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return busTypeId;
        }

        internal static int AddServiceStops_v2(int serviceId, string name, int cityId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddServiceStops_v2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlParameter LocationName = cmd.Parameters.Add("@LocationName", System.Data.SqlDbType.VarChar, 20);
                    LocationName.Direction = System.Data.ParameterDirection.Input;
                    LocationName.Value = name;

                    SqlParameter city = cmd.Parameters.Add("@CityId", System.Data.SqlDbType.Int);
                    city.Direction = System.Data.ParameterDirection.Input;
                    city.Value = cityId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = 0;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static void FailedExcelRecordValidation(ServiceExcelDTO serviceDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ServiceExcelRecords]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter rowId = cmd.Parameters.Add("@RowId", System.Data.SqlDbType.Int);
                    rowId.Direction = System.Data.ParameterDirection.Input;
                    rowId.Value = serviceDTO.RowId;

                    SqlParameter route = cmd.Parameters.Add("@Route", System.Data.SqlDbType.VarChar, 50);
                    route.Direction = System.Data.ParameterDirection.Input;
                    route.Value = serviceDTO.Route;

                    SqlParameter serviceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    serviceId.Direction = System.Data.ParameterDirection.Input;
                    serviceId.Value = serviceDTO.ServiceId;

                    SqlParameter departureTime = cmd.Parameters.Add("@DepartureTime", System.Data.SqlDbType.VarChar, 50);
                    departureTime.Direction = System.Data.ParameterDirection.Input;
                    departureTime.Value = serviceDTO.DeparuteTime;

                    SqlParameter arrivalTime = cmd.Parameters.Add("@ArrivalTime", System.Data.SqlDbType.VarChar, 50);
                    arrivalTime.Direction = System.Data.ParameterDirection.Input;
                    arrivalTime.Value = serviceDTO.DeparuteTime;

                    SqlParameter startLocation = cmd.Parameters.Add("@StartLocation", System.Data.SqlDbType.VarChar, 50);
                    startLocation.Direction = System.Data.ParameterDirection.Input;
                    startLocation.Value = serviceDTO.StartLocation;

                    SqlParameter endLocation = cmd.Parameters.Add("@EndLocation", System.Data.SqlDbType.VarChar, 50);
                    endLocation.Direction = System.Data.ParameterDirection.Input;
                    endLocation.Value = serviceDTO.EndLocation;

                    SqlParameter dayType = cmd.Parameters.Add("@DayType", System.Data.SqlDbType.VarChar, 50);
                    dayType.Direction = System.Data.ParameterDirection.Input;
                    dayType.Value = serviceDTO.DayType;

                    SqlParameter busType = cmd.Parameters.Add("@BusType", System.Data.SqlDbType.VarChar, 50);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = serviceDTO.BusType;

                    SqlParameter retailPrice = cmd.Parameters.Add("@RetailPrice", System.Data.SqlDbType.Decimal);
                    retailPrice.Direction = System.Data.ParameterDirection.Input;
                    retailPrice.Value = serviceDTO.RetailPrice;

                    SqlParameter specialPrice = cmd.Parameters.Add("@SpecialPrice", System.Data.SqlDbType.Decimal);
                    specialPrice.Direction = System.Data.ParameterDirection.Input;
                    specialPrice.Value = serviceDTO.SpecialPrice;

                    SqlParameter fromDate = cmd.Parameters.Add("@FromDate", System.Data.SqlDbType.DateTime);
                    fromDate.Direction = System.Data.ParameterDirection.Input;
                    fromDate.Value = serviceDTO.FromDate;

                    SqlParameter toDate = cmd.Parameters.Add("@ToDate", System.Data.SqlDbType.DateTime);
                    toDate.Direction = System.Data.ParameterDirection.Input;
                    toDate.Value = serviceDTO.ToDate;


                    SqlParameter startLocations = cmd.Parameters.Add("@StartLocations", System.Data.SqlDbType.VarChar, 1000);
                    startLocations.Direction = System.Data.ParameterDirection.Input;
                    startLocations.Value = serviceDTO.StartLocations2;

                    SqlParameter endLocations = cmd.Parameters.Add("@EndLocations", System.Data.SqlDbType.VarChar, 1000);
                    endLocations.Direction = System.Data.ParameterDirection.Input;
                    endLocations.Value = serviceDTO.EndLocations2;

                    SqlParameter validationErrors = cmd.Parameters.Add("@ValidationErrors", System.Data.SqlDbType.VarChar, 1000);
                    validationErrors.Direction = System.Data.ParameterDirection.Input;
                    validationErrors.Value = serviceDTO.ValidationErrorMessages;

                    SqlParameter merchantId = cmd.Parameters.Add("@MerchantId", System.Data.SqlDbType.Int);
                    merchantId.Direction = System.Data.ParameterDirection.Input;
                    merchantId.Value = serviceDTO.MerchantId;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = serviceDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = 0;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }

        internal static List<AdminUserDTO> GetAdminUsersList()
        {
            var admin = new List<AdminUserDTO>();

            try
            {
                var myConn = ConnectTODB();

                if (null != myConn)
                {
                    var cmd = new SqlCommand("[GetAdminUsers]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    var myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        var v = new AdminUserDTO
                        {
                            Id = myData["id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["id"]),
                            Name = myData["Name"] == DBNull.Value
                                ? ""
                                : Convert.ToString(myData["Name"].ToString()),
                            Email = myData["EmailAddress"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["EmailAddress"].ToString()),
                            Phone = myData["Phone"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["Phone"].ToString()),
                            Status = myData["Status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Status"].ToString()),
                            CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"])
                        };
                        admin.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return admin;
        }

        internal static int AddAdminUsers(AdminUserDTO adminUsersDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAdminUsers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 50);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = adminUsersDTO.Name;

                    SqlParameter phone = cmd.Parameters.Add("@Phone", System.Data.SqlDbType.VarChar, 50);
                    phone.Direction = System.Data.ParameterDirection.Input;
                    phone.Value = adminUsersDTO.Phone;

                    SqlParameter emailAddress = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    emailAddress.Direction = System.Data.ParameterDirection.Input;
                    emailAddress.Value = adminUsersDTO.Email;

                    SqlParameter status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = adminUsersDTO.Status;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = adminUsersDTO.CreatedBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = adminUsersDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@Id"].Value);


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }

        public static List<ServiceExcelDTO> GetFailedServiceHistory(int merchantId)
        {
            int id = 0;
            var serviceExcel = new List<ServiceExcelDTO>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    var cmd = new SqlCommand("[GetFailedRecordsByMerchant]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };
                    SqlParameter merchant = cmd.Parameters.Add("@MerchantId", System.Data.SqlDbType.Int);
                    merchant.Direction = System.Data.ParameterDirection.Input;
                    merchant.Value = merchantId;
                    var myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        List<string> list = new List<string>();

                        List<string> list2 = new List<string>();

                        if (myData["StartLocations"] != null)
                        {
                            string[] str1 = (Convert.ToString(myData["StartLocations"])).Split(',');
                            list = str1.ToList();
                        }
                        if (myData["EndLocations"] != null)
                        {
                            string[] str2 = (Convert.ToString(myData["EndLocations"])).Split(',');
                            list2 = str2.ToList();
                        }
                        var v = new ServiceExcelDTO
                        {
                            MerchantId = myData["MerchantId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["MerchantId"]),
                            RowId = myData["RowID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["RowID"]),
                            Route = myData["Route"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Route"]),
                            ServiceId = myData["ServiceId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ServiceId"]),
                            DeparuteTime = myData["DepartureTime"] == DBNull.Value ? string.Empty : Convert.ToString(myData["DepartureTime"]),
                            ArrivalTime = myData["ArrivalTime"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ArrivalTime"]),
                            StartLocation = myData["StartLocation"] == DBNull.Value ? string.Empty : Convert.ToString(myData["StartLocation"]),
                            EndLocation = myData["EndLocation"] == DBNull.Value ? string.Empty : Convert.ToString(myData["EndLocation"]),
                            DayType = myData["DayType"] == DBNull.Value ? string.Empty : Convert.ToString(myData["DayType"]),
                            BusType = myData["BusType"] == DBNull.Value ? string.Empty : Convert.ToString(myData["BusType"]),
                            RetailPrice = myData["RetailPrice"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(myData["RetailPrice"]),
                            SpecialPrice = myData["SpecialPrice"] == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(myData["SpecialPrice"]),
                            FromDate = myData["FromDate"] == DBNull.Value ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["FromDate"]),
                            ToDate = myData["ToDate"] == DBNull.Value ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["ToDate"]),
                            ValidationErrorMessages = myData["ValidationErrors"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ValidationErrors"]),
                            StartLocations = list,
                            EndLocations = list2,
                            CreatedDate = Convert.ToDateTime(myData["CreatedDate"])
                        };
                        serviceExcel.Add(v);
                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return serviceExcel;
        }

        internal static AdminUserDTO GetAdminUsersById(int Id)
        {
            AdminUserDTO admin = null;

            try
            {
                var myConn = ConnectTODB();

                if (null != myConn)
                {
                    var cmd = new SqlCommand("[GetAdminUsersById]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    var adminId = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    adminId.Direction = System.Data.ParameterDirection.Input;
                    adminId.Value = Id;

                    var myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        admin = new AdminUserDTO
                        {
                            Id = myData["id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["id"]),
                            Name = myData["Name"] == DBNull.Value
                                ? ""
                                : Convert.ToString(myData["Name"].ToString()),
                            Email = myData["EmailAddress"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["EmailAddress"].ToString()),
                            Phone = myData["Phone"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["Phone"].ToString()),
                            Status = myData["Status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Status"].ToString()),
                            CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"])
                        };

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return admin;
        }



        internal static void AddBusAmenities(int busTypeId, int amenityId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBusAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter busType = cmd.Parameters.Add("@BusTypeId", System.Data.SqlDbType.Int);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = busTypeId;

                    SqlParameter amenity = cmd.Parameters.Add("@AmenityId", System.Data.SqlDbType.Int);
                    amenity.Direction = System.Data.ParameterDirection.Input;
                    amenity.Value = amenityId;

                    cmd.ExecuteNonQuery();

                    // id = Convert.ToInt32(cmd.Parameters["@Id"].Value);

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        internal static List<Int32> GetAmenitiesByBusType(int busTypeId)
        {
            var list = new List<Int32>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBusAmenityId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter busType = cmd.Parameters.Add("@BusTypeId", System.Data.SqlDbType.Int);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = busTypeId;


                    var myData = cmd.ExecuteReader();

                    // id = Convert.ToInt32(cmd.Parameters["@Id"].Value);
                    while (myData.Read())
                    {
                        var aminityId = myData["AmenityId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["AmenityId"]);
                        list.Add(aminityId);
                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return list;
        }

        internal static int AddServiceAmenity(int serviceId, int amenityId, int createdBy)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddServiceAmenities]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ServiceId = cmd.Parameters.Add("@ServiceId", System.Data.SqlDbType.Int);
                    ServiceId.Direction = System.Data.ParameterDirection.Input;
                    ServiceId.Value = serviceId;

                    SqlParameter AmenityId = cmd.Parameters.Add("@AmenityId", System.Data.SqlDbType.Int);
                    AmenityId.Direction = System.Data.ParameterDirection.Input;
                    AmenityId.Value = amenityId;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    CreatedBy.Direction = System.Data.ParameterDirection.Input;
                    CreatedBy.Value = createdBy;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = 0;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        public static BusTypesDTO GetBusTypeById(int busTypeid)
        {
            BusTypesDTO busTypes = null;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBusTypeById]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    SqlParameter busType = cmd.Parameters.Add("@BusTypeId", System.Data.SqlDbType.Int);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = busTypeid;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        busTypes = new BusTypesDTO
                        {
                            Id = (int)myData["id"],
                            Name = myData["BusType"].ToString(),
                            CategoryId = (myData["CategoryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CategoryId"])
                        };
                    }
                    var list = GetAmenitiesById(busTypeid);
                    if (busTypes != null) busTypes.Amenities = list.Count > 0 ? list : null;
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return busTypes;
        }

        private static List<Int32> GetAmenitiesById(int busTypeId)
        {
            List<Int32> list = new List<Int32>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAmenitiesByBusTypeId]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    SqlParameter busType = cmd.Parameters.Add("@BusTypeId", System.Data.SqlDbType.Int);
                    busType.Direction = System.Data.ParameterDirection.Input;
                    busType.Value = busTypeId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        var id = myData["AmenityId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["AmenityId"]);
                        if (id > 0)
                        {
                            list.Add(id);
                        }
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return list;
        }

        internal static bool GetCustomerStatus(int customerId)
        {
            bool result = false;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTimeDiff]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CustomerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    CustomerId.Direction = System.Data.ParameterDirection.Input;
                    CustomerId.Value = customerId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    Int32 count = 0;
                    while (myData.Read())
                    {
                        count = (myData["TotalMinutes"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["TotalMinutes"]);
                    }
                    myData.Close();
                    myConn.Close();
                    if (count > 0 && count <= 300)
                    {
                        result = false;
                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }

        internal static List<BidDTO> GetBids_V1(int CustomerId)
        {
            List<BidDTO> Bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBids_V1]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter customerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    customerId.Direction = System.Data.ParameterDirection.Input;
                    customerId.Value = CustomerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidDTO v = new BidDTO();
                        ServiceDTO s = new ServiceDTO();
                        ServiceDaysDTO sd = new ServiceDaysDTO();
                        CustomerDTO customer = new CustomerDTO();
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        v.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["JourneyDate"];
                        v.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        v.Self = (myData["Self"] == DBNull.Value) ? true : (bool)myData["Self"];
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        v.ServiceDTO = s;
                        sd.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        v.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"];
                        v.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"];
                        v.ServiceDayDTO = sd;
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        customer.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        customer.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];
                        customer.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"];
                        customer.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"];
                        v.PhoneNumber = (myData["PhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["PhoneNumber"]);
                        v.TimeSlotType = (myData["TimeSlot"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TimeSlot"]);
                        //v.BusCategoryName = (myData["BusCategoryType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["BusCategoryType"]);
                        v.customerInfo = customer;
                        v.CustomerName = (myData["CustomerName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CustomerName"]);
                        Bids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Bids;
        }

        internal static List<CustomerOthersDto> GetCustomerOthers(int CustomerId)
        {
            List<CustomerOthersDto> CustomerOthers = new List<CustomerOthersDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCustomerOthers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter customerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    customerId.Direction = System.Data.ParameterDirection.Input;
                    customerId.Value = CustomerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CustomerOthersDto v = new CustomerOthersDto();

                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Name"]);
                        v.PhoneNumber = (myData["PhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["PhoneNumber"]);
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Gender"]);
                        v.EmailAddress = (myData["EmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                        CustomerOthers.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return CustomerOthers;
        }

        internal static int AddOtherCustomers(CustomerOthersDto customers)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCustomerOthers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 50);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = customers.Name;

                    SqlParameter phone = cmd.Parameters.Add("@PhoneNumber", System.Data.SqlDbType.VarChar, 20);
                    phone.Direction = System.Data.ParameterDirection.Input;
                    phone.Value = customers.PhoneNumber;

                    SqlParameter customer = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    customer.Direction = System.Data.ParameterDirection.Input;
                    customer.Value = customers.CustomerId;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", System.Data.SqlDbType.Int);
                    gender.Direction = System.Data.ParameterDirection.Input;
                    gender.Value = customers.Gender;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = customers.EmailAddress;


                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = 0;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int CheckUserExists(string emailAddress)
        {
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckUserExists]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = emailAddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        result = (myData["useroccurance"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["useroccurance"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<CustomerOthersDto> GetCustomerOthers_V1(int CustomerId)
        {
            List<CustomerOthersDto> CustomerOthers = new List<CustomerOthersDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetOtherCustomers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter customerId = cmd.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                    customerId.Direction = System.Data.ParameterDirection.Input;
                    customerId.Value = CustomerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CustomerOthersDto v = new CustomerOthersDto();

                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Name"]);
                        v.PhoneNumber = (myData["PhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["PhoneNumber"]);
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Gender"]);
                        v.EmailAddress = (myData["EmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                        CustomerOthers.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return CustomerOthers;
        }

        internal static List<BidDTO> GetLatestBid(int CustomerId)
        {
            List<BidDTO> Bids = new List<BidDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBids_V2]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter customerId = cmd.Parameters.Add("@CustomerId", System.Data.SqlDbType.Int);
                    customerId.Direction = System.Data.ParameterDirection.Input;
                    customerId.Value = CustomerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BidDTO v = new BidDTO();
                        ServiceDTO s = new ServiceDTO();
                        ServiceDaysDTO sd = new ServiceDaysDTO();
                        CustomerDTO customer = new CustomerDTO();
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.CustomerId = (myData["CustomerId"] == DBNull.Value) ? 0 : (int)myData["CustomerId"];
                        v.BidStatus = (myData["BidStatus"] == DBNull.Value) ? 0 : (int)myData["BidStatus"];
                        v.JourneyDate = (myData["JourneyDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["JourneyDate"];
                        v.Amount = (myData["Amount"] == DBNull.Value) ? 0 : (decimal)myData["Amount"];
                        v.Self = (myData["Self"] == DBNull.Value) ? true : (bool)myData["Self"];
                        v.NoOfSeats = (myData["NoOfSeats"] == DBNull.Value) ? 0 : (int)myData["NoOfSeats"];
                        s.FromTime = (myData["FromTime"] == DBNull.Value) ? "" : (string)myData["FromTime"];
                        s.ToTime = (myData["ToTime"] == DBNull.Value) ? "" : (string)myData["ToTime"];
                        s.ServiceNumber = (myData["ServiceNumber"] == DBNull.Value) ? 0 : (int)myData["ServiceNumber"];
                        v.ServiceDTO = s;
                        sd.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        v.FromCityName = (myData["FromCityName"] == DBNull.Value) ? "" : (string)myData["FromCityName"];
                        v.ToCityName = (myData["ToCityName"] == DBNull.Value) ? "" : (string)myData["ToCityName"];
                        v.ServiceDayDTO = sd;
                        v.BidStatusName = (myData["BidStatusName"] == DBNull.Value) ? "" : (string)myData["BidStatusName"];
                        customer.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        customer.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];
                        customer.Email = (myData["Email"] == DBNull.Value) ? "" : (string)myData["Email"];
                        customer.Phone = (myData["Phone"] == DBNull.Value) ? "" : (string)myData["Phone"];
                        v.PhoneNumber = (myData["PhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["PhoneNumber"]);
                        v.TimeSlotType = (myData["TimeSlot"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TimeSlot"]);
                        //v.BusCategoryName = (myData["BusCategoryType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["BusCategoryType"]);
                        v.customerInfo = customer;
                        v.CustomerName = (myData["CustomerName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CustomerName"]);
                        Bids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Bids;
        }
    }
}