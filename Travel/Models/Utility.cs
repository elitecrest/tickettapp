﻿using System;

namespace Travel
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }

        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception,
            UnmappedUniversityUser, // Exceptional only for when coach is not mapped to the university
            PaymentInProgress // Coach has not done the Payment
        }
        public enum UserType
        {
            Agent = 1,
            Vendor = 2,
            Admin = 3
        }

    }

    public static class CustomConstants
    {
        public static string User_Does_Not_Exist = "User does not exist";
        public static string User_Exist_With_Email = "User already exist";
        public static string InValid_Credential = "Invalid credential";
        public static string Registered_Successfully = "Registered successfully";
        public static string NoRecordsFound = "No records found";
        public static string Details_Get_Successfully = "Data retrieved successfully";
        public static string Already_Exists = "Record already exists";
        public static string BusBooking_Added_Successfully = "BusBooking done successfully";
        public static string BusBooking_Added_UnSuccessfully = "Record has not been added";
        public static string Inventory_Added_UnSuccessfully = "Inventory added successfully";
        public static string BusBooking_NoOfAttempts = "Maximum three attempts to change the price";
        public static string BusBooking_HourAttempts = "Only one attempt per an hour.";
        public static string Customer_Added_Successfully = "Customer added successfully";
        public static string AccessDenied = "Access Denied";
        public static string Vendor_Added_Successfully = "Vendor added successfully";
        public static string Bus_Added_Successfully = "Bus added successfully";
        public static string Bid_Added_Successfully = "Bid added successfully";
        public static string Service_Added_Successfully = "Service Saved successfully";
        public static string ServiceDays_Added_Successfully = "Service days Saved successfully";
        public static string ServiceAmenities_Added_Successfully = "Service amenities added successfully";
        public static string VendorDeals_Added_Successfully = "Vendor deals added successfully";
        public static string VendorRoute_Added_Successfully = "Vendor route added successfully";
        public static string Location_Added_Successfully = "Location Saved successfully";
        public static string ServiceStops_Added_Successfully = "Service stops added successfully";
        public static string Transaction_Added_Successfully = "Transaction added successfully";
        public static string MatchingBids_Added_Successfully = "Matching bids added successfully";
        public static string SeatNumber_Added_Successfully = "Seat number added successfully";
        public static string VendorUser_Added_Successfully = "Vendor user added successfully";
        public static string Log_In_Successful = "Login successful";
        public static string MailSent = "Mail sent successfully";
        public static string Email_Not_sent = "Mail has not been sent";
        public static string ResetPassword = "Password has been reset successfully";
        public static string UpdateUser_Update_Successfully = "User updated successfully";
        public static string Password_MisMatch = "Password mismatched";
        public static string Bid_Accepted = "Bid accepted successfully";
        public static string Record_Not_Inserted = "Record not inserted";
        public static string BusType_Added_Successfully = "Bustype Saved successfully";
        public static string City_Added_Successfully = "City Saved successfully";
        public static string BusType_Deleted = "Bus type has been deleted";
        public static string Route_Added_Successfully = "Route added successfully";
        public static string Route_Deleted = "Route deleted successfully";
        public static string Service_Deleted = "Service deleted successfully";
        public static string Seat_No_Allocated = "Seat number allocated";
        public static string Device_Registered_Successfully = "Device has been registered successfully";
        public static string Registered_Failed = "Registration Failed";
        public static string Exception_Added = "Exception added successfully";
        public static string Bus_Deleted = "Bus has been deleted successfully";
        public static string City_Deleted = "City has been deleted successfully";
        public static string Location_Deleted = "Location has been deleted successfully";
        public static string Merchant_Deleted = "Vendor has been deleted successfully";
        public static string Bid_Status_Updated_successfully = "Bid(s) status have been updated successfully";
        public static string NoRecords_On_JourneyDate = "None of your services have service running on the journeydate specified";
        public static string Record_Already_Exists = "Record already exists";
        public static string Agent_Deleted = "VendorUser has been deleted successfully";
        public static string VendorDeal_Deleted = "Vendor Deal has been deleted successfully";
        public static string Data_Saved_Successfully = "Data saved successfully";
        public static string ForgotPassword_successfully = "Mail sent successfully to registered email address";
        public static string ResetPassword_Failed = "Resetting password has been failed.Try again!";
        public static string Recieved = "Records recieved successfully";
        public static string Failed = "No Records Exists in the Database";
    }

    public static class ExcelUploadExceptionConstants
    {
        public static string RouteNotFound = "The Given Route Combination is not found";
        public static string StartLocationNotFound = "The Given Start Location is not associated with Given route's From City";
        public static string EndLocationNotFound = "The Given End Location is not associated with Given route's End City";
        public static string BusType = "The Given Bus Type is not exists in the system";
        public static string DayType = "The Given Day Type is not exists in the system";
        public static string StartPickupPointNotFound = "Source Pickup Point {0} is not Valid";
        public static string EndPickupPointNotFound = "Source Pickup Point {0} is not Valid";
        public static string Success = "All Records have been saved successfully.";
        public static string ValidationFailure = "Few/All records have invalid data, Only portion of the data is saved";
        public static string StartRouteNotFound = "The Given Start Route Combination is not found";
        public static string EndRouteNotFound = "The Given End Route Combination is not found";
        public static string StartSync = "Start location is not matching with the Route's From location ";
        public static string EndSync = "End location is not matching with the Route's To location ";
        public static string FromDate = "Invalid From Date for";
        public static string FromCityNotFound = "Invalid From City";
        public static string ToCityNotFound = "Invalid To City";
    }
}