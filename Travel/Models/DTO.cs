﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Travel.Models
{

    public partial class CityDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public Int32 CreatedBy { get; set; }
    }


    public partial class BusTypesDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 CreatedBy { get; set; }
        public Int32 IsUsed { get; set; }
        public Int32 CategoryId { get; set; }
        public List<Int32> Amenities { get; set; }
    }

    public partial class BusCategoriesDTO
    {
        public Int32 CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
    public partial class DayTypesDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }

    public partial class PaymentTypesDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }

    public partial class AmenitiesDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Decimal Cost { get; set; }
    }

    public partial class CustomerDTO
    {
        public Int32 Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String DOB { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public Int32 Gender { get; set; }
        public Int32 CityId { get; set; }
        public Int32 CreatedBy { get; set; }
        public String Password { get; set; }
        public string CityName { get; set; }
    }

    public partial class LocationDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String CityName { get; set; }
        public String CityState { get; set; }
        public String CityZip { get; set; }
        public Int32 CityId { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public Int32 CreatedBy { get; set; }
    }


    public partial class VendorDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String ContactName { get; set; }
        public String Phone1 { get; set; }
        public String Phone2 { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String Zip { get; set; }
        public String Fax { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public Int32 UserType { get; set; }
        public Int32 CreatedBy { get; set; }
        public String Base64String { get; set; }
        public String Format { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public String AccountNumber { get; set; }
        public String IFSC { get; set; }

    }

    public partial class VendorUserDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public Int32 VendorId { get; set; }
        public Int32 UserType { get; set; }
        public String Password { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public Int32 CreatedBy { get; set; }

    }

    public partial class BusDTO
    {
        public Int32 Id { get; set; }
        public Int32 NoOfSeats { get; set; }
        public Int32 IsAc { get; set; }
        public String SKU { get; set; }
        public Int32 BusTypeId { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public String BusType { get; set; }
        public Int32 CreatedBy { get; set; }
    }

    public partial class BidDTO
    {
        public Int32 Id { get; set; }
        public Int32 CustomerId { get; set; }
        public Int32 ServiceDayId { get; set; }
        public DateTime JourneyDate { get; set; }
        public Decimal Amount { get; set; }
        public Int32 BidStatus { get; set; }
        public Int32 NoOfSeats { get; set; }
        public Boolean Self { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public ServiceDaysDTO ServiceDayDTO { get; set; }
        public ServiceDTO ServiceDTO { get; set; }
        public Int32 fromCityId { get; set; }
        public Int32 toCityId { get; set; }
        public String BidStatusName { get; set; }
        public String FromCityName { get; set; }
        public String ToCityName { get; set; }
        public CustomerDTO customerInfo { get; set; }
        public Int32 RouteId { get; set; }
        public Int32 TotalCount { get; set; }
        public Int32 CreatedBy { get; set; }
        public String VendorName { get; set; }
        public string PhoneNumber { get; set; }
        public string TimeSlotType { get; set; }
        public string BusCategoryName { get; set; }
        public string CustomerName { get; set; }
    }


    public partial class ServiceDTO
    {
        public Int32 Id { get; set; }
        public Int32 RouteId { get; set; }
        public Int32 ServiceNumber { get; set; }
        public String FromTime { get; set; }
        public String ToTime { get; set; }
        public Int32 NoOfStops { get; set; }
        public Int32 DayTypeId { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Int32 VendorId { get; set; }
        public Int32 BusTypeId { get; set; }
        public Int32 StartLocation { get; set; }
        public Int32 EndLocation { get; set; }
        public Decimal RetailPrice { get; set; }
        public Decimal SpecialPrice { get; set; }
        public List<int> Locations { get; set; }
        public List<int> Amenities { get; set; }
        public String FromCityName { get; set; }
        public String ToCityName { get; set; }
        public Int32 serviceDay { get; set; }
        public List<int> DeleteLocations { get; set; }
        public List<int> DeleteAmenities { get; set; }
        public Int32 ServiceDayId { get; set; }
        public String BusType { get; set; }
        public Int32 CreatedBy { get; set; }
    }


    public partial class ServiceDaysDTO
    {
        public Int32 Id { get; set; }
        public Int32 ServiceId { get; set; }
        public DateTime Date { get; set; }
        public String Time { get; set; }
    }


    public partial class ServiceAmenitiesDTO
    {
        public Int32 Id { get; set; }
        public Int32 ServiceId { get; set; }
        public Int32 AmenityId { get; set; }
        public ServiceDTO ServiceDTO { get; set; }
        public AmenitiesDTO AmenitiesDTO { get; set; }
        public Int32 CreatedBy { get; set; }
    }

    public partial class ServiceStopsDTO
    {
        public Int32 Id { get; set; }
        public Int32 ServiceId { get; set; }
        public Int32 LocationId { get; set; }

    }

    public partial class VendorDealsDTO
    {
        public Int32 Id { get; set; }
        public Int32 ServiceId { get; set; }
        public DateTime Date { get; set; }
        public Int32 DiscountPercentage { get; set; }
        public Decimal OfferedPrice { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Int32 DayType { get; set; }
        public Int32 WeekDayName { get; set; } //When DayType = 5
        public Int32 AvailableSeats { get; set; }
        public Int32 RemainingSeats { get; set; }
        public Int32 CreatedBy { get; set; }

    }

    public partial class VendorRouteDTO
    {
        public Int32 Id { get; set; }
        public Int32 FromCity { get; set; }
        public Int32 ToCity { get; set; }
        public Int32 VendorId { get; set; }
        public Decimal RetailPrice { get; set; }
        public Decimal SpecialPrice { get; set; }
        public Int32 Distance { get; set; }
        public Int32 BusId { get; set; }
        public Int32 StartLocation { get; set; }
        public Int32 EndLocation { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public BusDTO Bus { get; set; }
        public String FromCityName { get; set; }
        public String ToCityName { get; set; }
        public String FromLocation { get; set; }
        public String ToLocation { get; set; }
        public String FromAddress { get; set; }
        public String ToAddress { get; set; }

    }

    public partial class TransactionDTO
    {
        public Int32 Id { get; set; }
        public Int32 PaymentTypeId { get; set; }
        public String VendorReferenceNo { get; set; }
        public String PaymentReferenceNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public Int32 BidId { get; set; }
        public Decimal Amount { get; set; }
        public Int32 Status { get; set; }
        public Int32 IsRefund { get; set; }
        public Decimal RefundAmount { get; set; }

    }

    public partial class MatchingBidsDTO
    {
        public Int32 Id { get; set; }
        public Int32 BidId { get; set; }
        public Int32 ServiceDayId { get; set; }
        public Int32 VendorDealId { get; set; }
        public string SeatNos { get; set; }
        public Int32 Status { get; set; }
        public Int32 CreatedBy { get; set; }
    }


    public partial class RouteDTO
    {
        public Int32 Id { get; set; }
        public Int32 FromCity { get; set; }
        public Int32 ToCity { get; set; }
        public Int32 Distance { get; set; }
        public Int32 Status { get; set; }
        public Int32 Type { get; set; }
        public String FromCityName { get; set; }
        public String ToCityName { get; set; }
        public Int32 CreatedBy { get; set; }
    }

    public partial class DeviceDTO
    {
        public Int32 Id { get; set; }
        public Int32 CustomerId { get; set; }
        public String UDID { get; set; }
        public String PushId { get; set; }
        public String DeviceType { get; set; }

    }

    public partial class RouteLocationsDTO
    {
        public List<LocationDTO> FromCityLocations { get; set; }
        public List<LocationDTO> ToCityLocations { get; set; }

    }

    public partial class ExceptionDTO
    {
        public Int64 Id { get; set; }
        public String Controller { get; set; }
        public String ActionMethod { get; set; }
        public String ExceptionText { get; set; }
        public String StackTrace { get; set; }
        public Int32 ExceptionRaisedTo { get; set; }
        public DateTime ExceptionDate { get; set; }

    }

    public partial class SeatDTO
    {
        public Int32 Id { get; set; }
        public String SeatNo { get; set; }
        public Int32 BidId { get; set; }
        public Int32 Status { get; set; }
        public Int32 CustomerId { get; set; }
        public String CustomerName { get; set; }
    }

    public partial class UpdateBidsDTO
    {
        public List<int> BidIds { get; set; }
        public int status { get; set; }
        public int ChangedBy { get; set; }
    }

    public partial class VendorPaymentDTO
    {
        public Int32 Id { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public String AccountNumber { get; set; }
        public String IFSC { get; set; }
        public Int32 CreatedBy { get; set; }
        public Int32 VendorId { get; set; }
    }

    public partial class BidStatisticsDTO
    {
        public Int32 Open { get; set; }
        public Int32 Processing { get; set; }
        public Int32 Processed { get; set; }
        public Int32 Completed { get; set; }
        public Int32 Archieved { get; set; }
        public Int32 UserId { get; set; }
    }

    public partial class CancelReasonsDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 Status { get; set; }

    }

    public partial class CancelBidsDTO
    {
        public Int32 BidId { get; set; }
        public Int32 ReasonId { get; set; }
        public String Description { get; set; }
        public Int32 CancelledBy { get; set; }
        public String CancelledName { get; set; }
        public Int32 UserType { get; set; }
        public DateTime CancelledOn { get; set; }
        public Int32 NoOfSeats { get; set; }
        public String ReasonName { get; set; }
        public String VendorName { get; set; }
    }

    public partial class ServiceExcelDTO
    {
        public string Route { get; set; }
        public int ServiceId { get; set; }
        public string DeparuteTime { get; set; }
        public string ArrivalTime { get; set; }
        public string StartLocation { get; set; }
        public string EndLocation { get; set; }
        public string DayType { get; set; }
        public string BusType { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal SpecialPrice { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<string> StartLocations { get; set; }
        public string StartLocations2 { get; set; }
        public string EndLocations2 { get; set; }
        public List<string> EndLocations { get; set; }
        public string ValidationErrorMessages { get; set; }
        public int RowId { get; set; }
        public int MerchantId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FromRoute { get; set; }
        public string ToRoute { get; set; }
        public int FromCityId { get; set; }
        public int ToCityId { get; set; }
        public int StartLocationId { get; set; }
        public int EndLocationId { get; set; }
        public int RouteId { get; set; }
        public int CreatedBy { get; set; }
        public int BusTypeId { get; set; }
    }

    public partial class AdminUserDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public Int32 Status { get; set; }
        public Int32 CreatedBy { get; set; }
    }

    public partial class LocationsDTO
    {
        public string LocationName { get; set; }
        public Int32 Status { get; set; }
    }

    public partial class ValidationSummaryDto
    {
        public Int32 RowId { get; set; }
        public string ValidationErrorMessage { get; set; }
    }

    public partial class CustomerOthersDto
    {
        public Int32 Id { get; set; }
        public Int32 CustomerId { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}